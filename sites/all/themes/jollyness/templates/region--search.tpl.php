<?php if ($content): ?>
  <span class="fa fa-search search-icon"></span>
  <div class="<?php print $classes; ?>">
    <?php print $content; ?>
    <span class="fa fa-times search-close"></span>
  </div>
<?php endif; ?>