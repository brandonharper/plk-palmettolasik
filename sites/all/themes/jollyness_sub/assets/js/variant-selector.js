// the semi-colon before function invocation is a safety net against concatenated
// scripts and/or other plugins which may not be closed properly.
;(function ( $ ) {

		// Create the defaults once
		var pluginName = "jqVarientSelector";

		// The actual plugin constructor
		function Plugin ( element ) {
			this._name = pluginName;
			this.init();
		}

		// Avoid Plugin.prototype conflicts
		$.extend(Plugin.prototype, {
			init: function () {
				var  _self = this,
					swatchOptions = $("#vsbox .swatches").children();
				
				_self.resetOptions($("#vsbox .swatches.control .selected"));
				$("#vsbox .swatches").each(function () {
					var selected = $(this).find(".selected img").attr("alt");
					$("<p/>").addClass("swatch-label").text(selected).insertAfter($(this));
				})

				// Set Options
				$("#vsbox .swatches.control").children().on("click", function () {
					_self.resetOptions($(this));
				});

				// Set Image
				swatchOptions.on("click", function () {
					var shell = $(this).data('shell') || '',
						cabinet = $(this).data('cabinet') || '';

					$(this).addClass("selected").siblings().removeClass("selected");

					if ( shell !== '') _self.setImage(shell, 'shell');
					if ( cabinet !== '') _self.setImage(cabinet, 'cabinet');
				});

				// Toggle Label on hover
				swatchOptions.on("mouseenter", function () {
					var selected = $(this).find("img").attr("alt");
					$(this).closest('.swatches').siblings('.swatch-label').text(selected);
				});
				swatchOptions.on("mouseleave", function () {
					var parent = $(this).closest('.swatches'),
						selected = parent.find(".selected img").attr("alt");
					parent.siblings('.swatch-label').text(selected);
				});
			},
			resetOptions: function (element) {
				var selectedShell = $("#vsbox .vs-image .shell:visible").data("shell"),
					swatches = $("#vsbox .swatches.options"),
					options = (typeof element.data("options") !== 'undefined') ? element.data("options").split(",") : new Array(),
					selector = '';

				$(options).each(function (index, value) {
					selector += "[data-shell='" + value + "'], ";
				});
				selector = selector.substring(0, selector.length - 2);

				swatches.children().hide();
				swatches.children(selector).show();

				// Set shell to first option
				if (!(swatches.children("[data-shell=" + selectedShell + "]").is(":visible"))) {
					swatches.children().removeClass("selected");
					swatches.children(":first").addClass("selected");
					this.setImage(swatches.children(":first").data("shell"), "shell");
				}
			},
			setImage: function (color, type) {
				$('#vsbox .vs-image img[data-' + type + ']').hide().removeClass("selected");
				$('#vsbox .vs-image img[data-' + type + '="' + color +'"]').show().addClass("selected");
			}
		});

		// A really lightweight plugin wrapper around the constructor,
		// preventing against multiple instantiations
		$.fn[ pluginName ] = function () {
			this.each(function() {
				if ( !$.data( this, "plugin_" + pluginName ) ) {
					$.data( this, "plugin_" + pluginName, new Plugin( this ) );
				}
			});
			return this;
		};


		$(document).ready(function() {
			$("#vsbox").jqVarientSelector();
		});
		
})( jQuery );