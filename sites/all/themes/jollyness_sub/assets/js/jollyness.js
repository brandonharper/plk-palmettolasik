jQuery(document).ready(function($) {
  $('.dexp-container').find('.block-big-title').wrap('<div class="container">');
  $('.search-icon').click(function(){
    $('.region-search').addClass('open').find('input[type=text]').focus();
  });
  $('.search-close').click(function(){
    $('.region-search').removeClass('open');
  });
  // Auto clear default value field
  $('.form-text,.form-textarea').cleardefault();
  // Tooltips 
  $('.bs-example-tooltips').tooltip({
    selector: "[data-toggle=tooltip]",
    container: "body"
  });
  $('.dtooltip').tooltip({
    container: 'body'
  });
  $("#bs-example a").popover();
 
  $(".stat-count").each(function() {
    //alert('ok');
    $(this).data('count', parseInt($(this).html(), 10));
    $(this).html('0');
    count($(this));
  });
  //Go to top
  $(window).scroll(function(){
    if($(this).scrollTop() > 100) {
      $('#go-to-top').css({
        bottom:"25px"
      });
    } else {
      $('#go-to-top').css({
        bottom:"-100px"
      });
    }
  });
  $('#go-to-top').click(function(){
    $('html, body').animate({
      scrollTop: '0px'
    }, 800);
    return false;
  });
 
});

function count($this) {
  var current = parseInt($this.html(), 10);
  current = current + 1; /* Where 50 is increment */

  $this.html(++current);
  if (current > $this.data('count')) {
    $this.html($this.data('count'));
  } else {
    setTimeout(function() {
      count($this)
    }, 50);
  }
}
jQuery.fn.cleardefault = function() {
  return this.focus(function() {
    if (this.value == this.defaultValue) {
      this.value = "";
    }
  }).blur(function() {
    if (!this.value.length) {
      this.value = this.defaultValue;
    }
  });
};

;(function ( $ ) {

		// Create the defaults once
		var pluginName = "jqVarientSelector";

		// The actual plugin constructor
		function Plugin ( element ) {
			this._name = pluginName;
			this.init();
		}

		// Avoid Plugin.prototype conflicts
		$.extend(Plugin.prototype, {
			init: function () {
				var  _self = this,
					swatchOptions = $("#vsbox .swatches").children();
				
				_self.resetOptions($("#vsbox .swatches.control .selected"));
				$("#vsbox .swatches").each(function () {
					var selected = $(this).find(".selected img").attr("alt");
					$("<p/>").addClass("swatch-label").text(selected).insertAfter($(this));
				})

				// Set Options
				$("#vsbox .swatches.control").children().on("click", function () {
					_self.resetOptions($(this));
				});

				// Set Image
				swatchOptions.on("click", function () {
					var shell = $(this).data('shell') || '',
						cabinet = $(this).data('cabinet') || '';

					$(this).addClass("selected").siblings().removeClass("selected");

					if ( shell !== '') _self.setImage(shell, 'shell');
					if ( cabinet !== '') _self.setImage(cabinet, 'cabinet');
				});

				// Toggle Label on hover
				swatchOptions.on("mouseenter", function () {
					var selected = $(this).find("img").attr("alt");
					$(this).closest('.swatches').siblings('.swatch-label').text(selected);
				});
				swatchOptions.on("mouseleave", function () {
					var parent = $(this).closest('.swatches'),
						selected = parent.find(".selected img").attr("alt");
					parent.siblings('.swatch-label').text(selected);
				});
			},
			resetOptions: function (element) {
				var selectedShell = $("#vsbox .vs-image .shell:visible").data("shell"),
					swatches = $("#vsbox .swatches.options"),
					options = (typeof element.data("options") !== 'undefined') ? element.data("options").split(",") : new Array(),
					selector = '';

				$(options).each(function (index, value) {
					selector += "[data-shell='" + value + "'], ";
				});
				selector = selector.substring(0, selector.length - 2);

				swatches.children().hide();
				swatches.children(selector).show();

				// Set shell to first option
				if (!(swatches.children("[data-shell=" + selectedShell + "]").is(":visible"))) {
					swatches.children().removeClass("selected");
					swatches.children(":first").addClass("selected");
					this.setImage(swatches.children(":first").data("shell"), "shell");
				}
			},
			setImage: function (color, type) {
				$('#vsbox .vs-image img[data-' + type + ']').hide().removeClass("selected");
				$('#vsbox .vs-image img[data-' + type + '="' + color +'"]').show().addClass("selected");
			}
		});

		// A really lightweight plugin wrapper around the constructor,
		// preventing against multiple instantiations
		$.fn[ pluginName ] = function () {
			this.each(function() {
				if ( !$.data( this, "plugin_" + pluginName ) ) {
					$.data( this, "plugin_" + pluginName, new Plugin( this ) );
				}
			});
			return this;
		};


		$(document).ready(function() {
			$("#vsbox").jqVarientSelector();
		});
		
})( jQuery );