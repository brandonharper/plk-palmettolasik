/**
 * Created with JetBrains PhpStorm.
 * User: rickenbachn
 * Date: 3/5/13
 * Time: 1:30 PM
 * To change this template use File | Settings | File Templates.
 */

$(function () {
    $('#mainHeader .header_safety button').click(function (e) {
        e.preventDefault();
        $('#mainNav').toggleClass('active');
    })
});

var ScrollWindow = function (target, offset) {
    offset = offset || 0;
    if (typeof target == 'string') target = $(target);
    $('html,body').animate({
        scrollTop: target.offset().top - $('#mainHeader').height() + offset
    }, 1000);
};



// FANCY RADIO BUTTONS

var fancyRadioButtons = {};

$(function () {
    var radioGroups = [];

    fancyRadioButtons.convert = function (target, id) {
        if (typeof target == "string") {
            $("input[name=" + target + "]").each(function () {
                fancyRadioButtons.convert($(this));
            })
        }
        else {
            if (target.parent().hasClass('fancyRadioButton')) return;
            var groupName = target.attr('name');
            target
                .wrap("<div class='fancyRadioButton" + (target.prop('checked') ? " active" : "") + "' data-radio-group='" + groupName + "'></div>")
                .after("<a" + (id ? " " + id : "") + " href='#'></a>");
            if (!radioGroups[groupName])
                radioGroups[groupName] = $();
            radioGroups[groupName] = radioGroups[groupName].add(target.parent());
        }
    };

    fancyRadioButtons.select = function (e) {
        e.preventDefault();
        var target = $(this).parent();
        var groupName = target.attr('data-radio-group');
        radioGroups[groupName].removeClass('active').find('input').removeAttr('checked');
        target.addClass('active');
        target.find('input')
            .prop('checked', true)
            .attr('checked', 'checked');

        if (calc) { calc.getContactCost(); }
        if (quiz) { quiz.fadeFactBox(target.find('input').attr('value')); }
    };

    $('body').on('click', '.fancyRadioButton a', fancyRadioButtons.select);
    $('body').on('change', '.fancyRadioButton input', fancyRadioButtons.select);

    $('.makeFancy').each(function () {
        fancyRadioButtons.convert($(this).removeClass('makeFancy'));
    });
});



// FANCY SELECT BOX

var fancySelectBoxes = {};

$(function () {

    fancySelectBoxes.convert = function (_id, _choice, _big) {
        var target = (typeof _id == 'string') ? $(_id) : _id;

        var big = (_big);
        var options = [];
        var chosen = 0;
        var num = 0;
        var elementId = $(_id).attr('id');

        target.children("option").each(function () {
            if(_choice){
                if($(this).val()==_choice){
                    chosen = num;
                    $(this).attr("selected","selected");
                }else{
                    $(this).removeAttr("selected");
                }
            }else if($(this).attr("selected")=="selected"){
                chosen = num;
            }
            num++;
            options.push({name:$(this).html(),val:$(this).val()});
        });

        var container = target.wrap("<div id='" + elementId + "_box' class='" + (big ? "selectBigBox" : "selectBox") + (target.hasClass("long") ? " long" : "") + "'>").parent();

        var optionName = options[chosen].name;
        if(options[chosen].name.indexOf("months")>-1){
            var opts = optionName.split(" ");
            optionName = opts[0]+" <span class='mon'>"+opts[1]+"</span>";
        }

        container.append("<a href='#' class='selectHeader'><span class='selected-option'>" + optionName + "</span><span class='headerArrows'></span></a>");

        var list = $("<div id='" + elementId + "_options' class='selectOptionBox'></div>");
        for (var i = 0; i < options.length; i++) {
            optionName = options[i].name;
            if(options[i].name.indexOf("months")>-1){
                opts = optionName.split(" ");
                optionName = opts[0]+" <span class='mon'>"+opts[1]+"</span>";
            }

            list.append("<a href='#' class='option' data-val='" + options[i].val + "'><span class='option-text'>" + optionName + "</span></a>");
        }
        container.append(list);

        //copy classes from select box to new  custom element
        container.addClass(target.attr('class'));
    }

    fancySelectBoxes.toggle = function (_id) {
        var target = (typeof _id == 'string') ? $(_id) : _id;
        target.find('.selectOptionBox').slideToggle(function () { $(this).css('overflow', ''); });
    }

    fancySelectBoxes.select = function (_id) {
        var target = (typeof _id == 'string') ? $(_id) : _id;
        var container = target.parents('.selectBox, .selectBigBox');
        var _name = "";
        var _val = target.attr('data-val');
        container.find("option").each(function () {
            if($(this).val()==_val){
                _name = $(this).html();
                $(this).attr("selected","selected");
                $(this).val(_val).change(); //change event ensures AngularJS picks up the change and applies it
            }else{
                $(this).removeAttr("selected");
            }
        });
        var optionName = _name;
        if(optionName.indexOf("months")>-1){
            var opts = optionName.split(" ");
            optionName = opts[0]+" <span class='mon'>"+opts[1]+"</span>";
        }

        container.find(".selected-option").html(optionName);
        if(calc){ calc.getContactCost(); }
        fancySelectBoxes.toggle(container);
    }

    $('body').on('click', '.selectHeader', function (e) {
        e.preventDefault();
        fancySelectBoxes.toggle($(this).parent());
    });

    $('body').on('click', '.selectOptionBox .option', function (e) {
        e.preventDefault();
        fancySelectBoxes.select($(this));
    });
});








function jumpToBottom() {
    ScrollWindow('#safetyInfo');
}

function adjustNav(_section){
    curSection = _section;
    $(".nav_but[rel='" + curSection + "']").addClass('active');
}



/************  FLOODLIGHT TAGS  *************/

function sendDart(cat){
    var frame = $("<iframe></iframe>");
    var axel = Math.random() + "";
    var a = axel * 10000000000000;
    frame.attr("id","DCLK_FLIframe_"+Math.floor(Math.random()*999999));
    frame.attr("src","http://2644366.fls.doubleclick.net/activityi;src=2644366;type=medic850;cat="+cat+";ord=" + a + "?");
    $("#flood_tags").append(frame);
}



/*********** VIDEO POP  *************/

var vPlayer = {};

$(function () {

    var videoStart = function (id) {
        vidStart = true;
        vidHalfTracked = false;
        vidFullTracked = false;
        //_gaq.push(['_trackEvent', 'Button', 'Click', 'Home | Launch Video']);
        var vidFrame = '<iframe src="http://player.vimeo.com/video/'+id+'?autoplay=1&api=1&player_id=vimeoplayer" frameborder="0" id="vimeoplayer" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>';
        $("#video").html(vidFrame);

        vPlayer.container.modal('show');

        player = $f($("#vimeoplayer")[0]);
        player.addEvent('ready', function() {
            //status.text('ready');

            player.addEvent('finish', onFinish);
            player.addEvent('pause', onPause);
            player.addEvent('play', onPlay);
            player.addEvent('ready', onReady);
            player.addEvent('playProgress', onPlayProgress);
        });
        $("#vid_overlay").focus();
    }

    var onPlayProgress = function (data, id) {

        //track halfway at 30 seconds
        if (data.seconds > 30 && !vidHalfTracked) {
            trackEvent('Video', '50% Video', 'Homepage Video');
            vidHalfTracked = true;
        }

        //track finished at 60 seconds
        if (data.seconds > 60 && !vidFullTracked) {
            trackEvent('Video', 'Finish Video', 'Homepage Video');
            vidFullTracked = true;
        }
    }

    var onFinish = function (){
        vidClose(true);
        if(!IS_DEV){
            player.api('getCurrentTime', function (value, player_id) {
                sc_omniMediaTrackingComplete("liveyourlife",value);
            });
        }
        //_gaq.push(['_trackEvent', 'Video', 'Click',  'Home | Finish']);
    }

    var onPause = function (){
        //_gaq.push(['_trackEvent', 'Video', 'Click',  'Home | Pause']);
        if(!IS_DEV){
            player.api('getCurrentTime', function (value, player_id) {
                sc_omniMediaTrackingPause("liveyourlife",value);
            });
        }
    }

    var onPlay = function (){
        if(vidStart){
            vidStart = false;
            trackEvent('Video', 'Start Video', 'Homepage Video');
            return;
        }else{
            //_gaq.push(['_trackEvent', 'Video', 'Click',  'Home | Play']);
            if(!IS_DEV){
                player.api('getCurrentTime', function (value, player_id) {
                    sc_omniMediaTrackingResume("liveyourlife",value);
                });
            }
        }
    }

    var onReady = function (){
        //_gaq.push(['_trackEvent', 'Video', 'Click',  'Home | Loaded']);
        player.api('getDuration', function (value, player_id) {

            vidDuration = value;

            if (!IS_DEV) {
                sc_omniStartMediaTracking("liveyourlife", value, "vimeo");
            }
        });
    }

    vPlayer.container = $('#vid_container');
    vPlayer.container
        .modal({ show: false })
        .on('hidden.bs.modal', function (e) {
            $("#video").empty();
        });

    vPlayer.hide = function () {
        vPlayer.container.modal('hide');
    }

    $('#vid_play_but').click(function (e) {
        e.preventDefault();
        videoStart('62906459');
    });
});

var froogaloop=function(){function e(a){return new e.fn.init(a)}function h(a,c,b){if(!b.contentWindow.postMessage)return!1;var f=b.getAttribute("src").split("?")[0],a=JSON.stringify({method:a,value:c});"//"===f.substr(0,2)&&(f=window.location.protocol+f);b.contentWindow.postMessage(a,f)}function j(a){var c,b;try{c=JSON.parse(a.data),b=c.event||c.method}catch(f){}"ready"==b&&!i&&(i=!0);if(a.origin!=k)return!1;var a=c.value,e=c.data,g=""===g?null:c.player_id;c=g?d[g][b]:d[b];b=[];if(!c)return!1;void 0!==
    a&&b.push(a);e&&b.push(e);g&&b.push(g);return 0<b.length?c.apply(null,b):c.call()}function l(a,c,b){b?(d[b]||(d[b]={}),d[b][a]=c):d[a]=c}var d={},i=!1,k="";e.fn=e.prototype={element:null,init:function(a){"string"===typeof a&&(a=document.getElementById(a));this.element=a;a=this.element.getAttribute("src");"//"===a.substr(0,2)&&(a=window.location.protocol+a);for(var a=a.split("/"),c="",b=0,f=a.length;b<f;b++){if(3>b)c+=a[b];else break;2>b&&(c+="/")}k=c;return this},api:function(a,c){if(!this.element||
    !a)return!1;var b=this.element,f=""!==b.id?b.id:null,d=!c||!c.constructor||!c.call||!c.apply?c:null,e=c&&c.constructor&&c.call&&c.apply?c:null;e&&l(a,e,f);h(a,d,b);return this},addEvent:function(a,c){if(!this.element)return!1;var b=this.element,d=""!==b.id?b.id:null;l(a,c,d);"ready"!=a?h("addEventListener",a,b):"ready"==a&&i&&c.call(null,d);return this},removeEvent:function(a){if(!this.element)return!1;var c=this.element,b;a:{if((b=""!==c.id?c.id:null)&&d[b]){if(!d[b][a]){b=!1;break a}d[b][a]=null}else{if(!d[a]){b=
    !1;break a}d[a]=null}b=!0}"ready"!=a&&b&&h("removeEventListener",a,c)}};e.fn.init.prototype=e.fn;window.addEventListener?window.addEventListener("message",j,!1):window.attachEvent("onmessage",j);return window.Froogaloop=window.$f=e}();
