function Calculator(_opts){

    //GREEN TXT
    var CLC,
        IPC,
        IPCM1,
        IPCM1c,
        IPCM1l,
        IPCM2,
        IPCM2c,
        IPCM2l,
        IPCM3,
        IPCM3c,
        IPCM3l;

    //INPUT TXT
    var ageRange,
        contactType,
        procedureCost,
        insurance,
        payOff,
        downPayment;

    this.makeOptions = function(_opts){
        for(x in _opts){
            eval(x+" = "+_opts[x]);
        }
    }

    this.getContactCost = function(){
        var age = ageRange.val();
        //console.log(contactType);
        var type = $("input[name='"+contactType+"']:checked").val();
        var cost = formatCurrency(calculateCost(age,type));
        CLC.html(cost);
        CLC.stop().css({opacity:0.1}).animate({opacity:1},300);
    }

    this.calculateMonthlyPayment = function(){
        var cost = procedureCost.val().replace("$","").replace(",","");
        var insur = insurance.val().replace("%","");
        var downP = downPayment.val().replace("$","").replace(",","");
        var months = Number(payOff.val());

        var M1;
        var M2;
        var M3;

        var monthsArr = [6,12,18,24];
        var loc = findMonth(months,monthsArr);
        if(loc==0){
            M1 = monthsArr[0];
            M2 = monthsArr[1];
            M3 = monthsArr[2];
        }else if(loc==monthsArr.length-1){
            M1 = monthsArr[monthsArr.length-3];
            M2 = monthsArr[monthsArr.length-2];
            M3 = monthsArr[monthsArr.length-1];
        }else{
            M1 = monthsArr[loc-1];
            M2 = monthsArr[loc];
            M3 = monthsArr[loc+1];
        }

        var total = cost - (cost*(insur/100)) - downP;

        if(total<0){

            downPayment.css({border:"1px solid #ff0000"});

        }else{

            downPayment.css({border:"1px solid #cccccc"});

            var m1 = Math.round((total/M1)*100)/100;
            var m1c = (String(m1).split(".")[1]) ? "."+String(m1).split(".")[1] : ".00";
            m1c = (m1c.length<2) ? m1c+"0" : m1c;
            var m2 = Math.round((total/M2)*100)/100;
            var m2c = (String(m2).split(".")[1]) ? "."+String(m2).split(".")[1] : ".00";
            m2c = (m2c.length<2) ? m2c+"0" : m2c;
            var m3 = Math.round((total/M3)*100)/100;
            var m3c = (String(m3).split(".")[1]) ? "."+String(m3).split(".")[1] : ".00";
            m3c = (m3c.length<2) ? m3c+"0" : m3c;

            IPCM1.html(formatCurrency(String(m1).split(".")[0])).stop().css({opacity:0.1}).animate({opacity:1},300);
            IPCM1c.html(m1c).stop().css({opacity:0.1}).animate({opacity:1},300);
            IPCM1l.html(M1);
            IPCM2.html(formatCurrency(String(m2).split(".")[0])).stop().css({opacity:0.1}).animate({opacity:1},300);
            IPCM2c.html(m2c).stop().css({opacity:0.1}).animate({opacity:1},300);
            IPCM2l.html(M2);
            IPCM3.html(formatCurrency(String(m3).split(".")[0])).stop().css({opacity:0.1}).animate({opacity:1},300);
            IPCM3c.html(m3c).stop().css({opacity:0.1}).animate({opacity:1},300);
            IPCM3l.html(M3);

            sendDart("costp721");
            if(!IS_DEV){
                sc_trackingType = "onclick";
                sc_toolCostCalculatorStage = "results";
                sc_toolCostCalculatorAttributes = total;
                sc_sendCostCalculatorAnalytics(sc_trackingType,sc_toolCostCalculatorStage,sc_toolCostCalculatorAttributes);
            }
            //_gaq.push(['_trackEvent', "Cost Calculation", 'Load', "Total ("+total+")"]);

            trackEvent('Cost Calculator', 'Click Calculate Button');
        }
    }

    function findMonth(mnth,arr){
        var loc = -1;
        for(var i=0;i<arr.length;i++){
            if(arr[i]==mnth){
                loc = i;
                break;
            }
        }
        return loc;
    }

    function calculateCost(_age, _contactType){
        //Set multiplier
        //console.log(_age,_contactType);
        var multiplier = 0;

        if (_age == 1){
            multiplier = 2;
        }else if (_age == 2){
            multiplier = 1.5
        }else if (_age == 3){
            multiplier = 1;
        }

        //Get base cost

        var baseCost = 0;

        if (_contactType == 1){
            baseCost = 12340;
        }else if (_contactType == 2){
            baseCost = 14600;
        }

        //trackCalc(age, contactType);
        //calculate the final cost

        return baseCost * multiplier;
    }

    function formatCurrency(amount){
        var formatted = amount.toString();

        var regCommas = /(\d+)(\d{3})/;

        while (regCommas.test(formatted)){
            formatted = formatted.replace(regCommas, "$1" + "," + "$2");
        }

        return "$" + formatted;
    }
}