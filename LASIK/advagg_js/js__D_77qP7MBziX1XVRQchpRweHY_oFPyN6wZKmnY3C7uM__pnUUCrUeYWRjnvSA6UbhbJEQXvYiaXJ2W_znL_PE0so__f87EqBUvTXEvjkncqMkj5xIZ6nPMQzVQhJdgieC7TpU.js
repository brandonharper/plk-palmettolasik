(function ($) {

/**
 * Retrieves the summary for the first element.
 */
$.fn.drupalGetSummary = function () {
  var callback = this.data('summaryCallback');
  return (this[0] && callback) ? $.trim(callback(this[0])) : '';
};

/**
 * Sets the summary for all matched elements.
 *
 * @param callback
 *   Either a function that will be called each time the summary is
 *   retrieved or a string (which is returned each time).
 */
$.fn.drupalSetSummary = function (callback) {
  var self = this;

  // To facilitate things, the callback should always be a function. If it's
  // not, we wrap it into an anonymous function which just returns the value.
  if (typeof callback != 'function') {
    var val = callback;
    callback = function () { return val; };
  }

  return this
    .data('summaryCallback', callback)
    // To prevent duplicate events, the handlers are first removed and then
    // (re-)added.
    .unbind('formUpdated.summary')
    .bind('formUpdated.summary', function () {
      self.trigger('summaryUpdated');
    })
    // The actual summaryUpdated handler doesn't fire when the callback is
    // changed, so we have to do this manually.
    .trigger('summaryUpdated');
};

/**
 * Sends a 'formUpdated' event each time a form element is modified.
 */
Drupal.behaviors.formUpdated = {
  attach: function (context) {
    // These events are namespaced so that we can remove them later.
    var events = 'change.formUpdated click.formUpdated blur.formUpdated keyup.formUpdated';
    $(context)
      // Since context could be an input element itself, it's added back to
      // the jQuery object and filtered again.
      .find(':input').andSelf().filter(':input')
      // To prevent duplicate events, the handlers are first removed and then
      // (re-)added.
      .unbind(events).bind(events, function () {
        $(this).trigger('formUpdated');
      });
  }
};

/**
 * Prepopulate form fields with information from the visitor cookie.
 */
Drupal.behaviors.fillUserInfoFromCookie = {
  attach: function (context, settings) {
    $('form.user-info-from-cookie').once('user-info-from-cookie', function () {
      var formContext = this;
      $.each(['name', 'mail', 'homepage'], function () {
        var $element = $('[name=' + this + ']', formContext);
        var cookie = $.cookie('Drupal.visitor.' + this);
        if ($element.length && cookie) {
          $element.val(cookie);
        }
      });
    });
  }
};

})(jQuery);
;/**/
(function ($) {

/**
 * Toggle the visibility of a fieldset using smooth animations.
 */
Drupal.toggleFieldset = function (fieldset) {
  var $fieldset = $(fieldset);
  if ($fieldset.is('.collapsed')) {
    var $content = $('> .fieldset-wrapper', fieldset).hide();
    $fieldset
      .removeClass('collapsed')
      .trigger({ type: 'collapsed', value: false })
      .find('> legend span.fieldset-legend-prefix').html(Drupal.t('Hide'));
    $content.slideDown({
      duration: 'fast',
      easing: 'linear',
      complete: function () {
        Drupal.collapseScrollIntoView(fieldset);
        fieldset.animating = false;
      },
      step: function () {
        // Scroll the fieldset into view.
        Drupal.collapseScrollIntoView(fieldset);
      }
    });
  }
  else {
    $fieldset.trigger({ type: 'collapsed', value: true });
    $('> .fieldset-wrapper', fieldset).slideUp('fast', function () {
      $fieldset
        .addClass('collapsed')
        .find('> legend span.fieldset-legend-prefix').html(Drupal.t('Show'));
      fieldset.animating = false;
    });
  }
};

/**
 * Scroll a given fieldset into view as much as possible.
 */
Drupal.collapseScrollIntoView = function (node) {
  var h = document.documentElement.clientHeight || document.body.clientHeight || 0;
  var offset = document.documentElement.scrollTop || document.body.scrollTop || 0;
  var posY = $(node).offset().top;
  var fudge = 55;
  if (posY + node.offsetHeight + fudge > h + offset) {
    if (node.offsetHeight > h) {
      window.scrollTo(0, posY);
    }
    else {
      window.scrollTo(0, posY + node.offsetHeight - h + fudge);
    }
  }
};

Drupal.behaviors.collapse = {
  attach: function (context, settings) {
    $('fieldset.collapsible', context).once('collapse', function () {
      var $fieldset = $(this);
      // Expand fieldset if there are errors inside, or if it contains an
      // element that is targeted by the URI fragment identifier.
      var anchor = location.hash && location.hash != '#' ? ', ' + location.hash : '';
      if ($fieldset.find('.error' + anchor).length) {
        $fieldset.removeClass('collapsed');
      }

      var summary = $('<span class="summary"></span>');
      $fieldset.
        bind('summaryUpdated', function () {
          var text = $.trim($fieldset.drupalGetSummary());
          summary.html(text ? ' (' + text + ')' : '');
        })
        .trigger('summaryUpdated');

      // Turn the legend into a clickable link, but retain span.fieldset-legend
      // for CSS positioning.
      var $legend = $('> legend .fieldset-legend', this);

      $('<span class="fieldset-legend-prefix element-invisible"></span>')
        .append($fieldset.hasClass('collapsed') ? Drupal.t('Show') : Drupal.t('Hide'))
        .prependTo($legend)
        .after(' ');

      // .wrapInner() does not retain bound events.
      var $link = $('<a class="fieldset-title" href="#"></a>')
        .prepend($legend.contents())
        .appendTo($legend)
        .click(function () {
          var fieldset = $fieldset.get(0);
          // Don't animate multiple times.
          if (!fieldset.animating) {
            fieldset.animating = true;
            Drupal.toggleFieldset(fieldset);
          }
          return false;
        });

      $legend.append(summary);
    });
  }
};

})(jQuery);
;/**/
/**
 * @file
 * Javascript functionality for Display Suite's administration UI.
 */

(function($) {

Drupal.DisplaySuite = Drupal.DisplaySuite || {};
Drupal.DisplaySuite.fieldopened = '';
Drupal.DisplaySuite.layout_original = '';

/**
 * Ctools selection content.
 */
Drupal.behaviors.CToolsSelection = {
  attach: function (context) {
    if ($('#ctools-content-selection').length > 0) {
      $('#ctools-content-selection .section-link').click(function() {
        $('#ctools-content-selection .content').hide();
        container = $(this).attr('id') + '-container';
        $('#' + container).show();
        return false;
      });
    }
  }
};

/**
 * Save the Dynamic field content configuration.
 */
$.fn.dsCtoolsContentConfiguration = function (configuration) {
  $(this[0]).val(configuration);
}

/**
 * Update the select content text.
 */
$.fn.dsCtoolsContentUpdate = function () {
  $(this[0]).html(Drupal.t('Click update to save the configuration'));
}

/**
 * Save the page after saving a new field.
 */
$.fn.dsRefreshDisplayTable = function () {
  $('#edit-submit').click();
}

/**
 * Row handlers for the 'Manage display' screen.
 */
Drupal.fieldUIDisplayOverview = Drupal.fieldUIDisplayOverview || {};

Drupal.fieldUIDisplayOverview.ds = function (row, data) {

  this.row = row;
  this.name = data.name;
  this.region = data.region;
  this.tableDrag = data.tableDrag;

  // Attach change listener to the 'region' select.
  this.$regionSelect = $('select.ds-field-region', row);
  this.$regionSelect.change(Drupal.fieldUIOverview.onChange);

  // Attach change listener to the 'formatter type' select.
  this.$formatSelect = $('select.field-formatter-type', row);
  this.$formatSelect.change(Drupal.fieldUIOverview.onChange);

  return this;
};

Drupal.fieldUIDisplayOverview.ds.prototype = {

  /**
   * Returns the region corresponding to the current form values of the row.
   */
  getRegion: function () {
    return this.$regionSelect.val();
  },

  /**
   * Reacts to a row being changed regions.
   *
   * This function is called when the row is moved to a different region, as a
   * result of either :
   * - a drag-and-drop action
   * - user input in one of the form elements watched by the
   *   Drupal.fieldUIOverview.onChange change listener.
   *
   * @param region
   *   The name of the new region for the row.
   * @return
   *   A hash object indicating which rows should be AJAX-updated as a result
   *   of the change, in the format expected by
   *   Drupal.displayOverview.AJAXRefreshRows().
   */
  regionChange: function (region) {

     // Replace dashes with underscores.
     region = region.replace(/-/g, '_');

     // Set the region of the select list.
     this.$regionSelect.val(region);

     // Prepare rows to be refreshed in the form.
     var refreshRows = {};
     refreshRows[this.name] = this.$regionSelect.get(0);

     // If a row is handled by field_group module, loop through the children.
     if ($(this.row).hasClass('field-group') && $.isFunction(Drupal.fieldUIDisplayOverview.group.prototype.regionChangeFields)) {
       Drupal.fieldUIDisplayOverview.group.prototype.regionChangeFields(region, this, refreshRows);
     }

     return refreshRows;
  }
};

})(jQuery);
;/**/

(function($) {

Drupal.behaviors.fieldUIFieldsOverview = {
  attach: function (context, settings) {
    $('table#field-overview', context).once('field-field-overview', function() {
      Drupal.fieldUIOverview.attach(this, settings.fieldUIRowsData, Drupal.fieldUIFieldOverview);
    });
  }
};
  
/**
 * Row handlers for the 'Manage fields' screen.
 */
Drupal.fieldUIFieldOverview = Drupal.fieldUIFieldOverview || {};

Drupal.fieldUIFieldOverview.group = function(row, data) {
  this.row = row;
  this.name = data.name;
  this.region = data.region;
  this.tableDrag = data.tableDrag;

  // Attach change listener to the 'group format' select.
  this.$formatSelect = $('select.field-group-type', row);
  this.$formatSelect.change(Drupal.fieldUIOverview.onChange);

  return this;
};

Drupal.fieldUIFieldOverview.group.prototype = {
  getRegion: function () {
    return 'main';
  },
  
  regionChange: function (region, recurse) {
    return {};
  },

  regionChangeFields: function (region, element, refreshRows) {

    // Create a new tabledrag rowObject, that will compute the group's child
    // rows for us.
    var tableDrag = element.tableDrag;
    rowObject = new tableDrag.row(element.row, 'mouse', true);
    // Skip the main row, we handled it above.
    rowObject.group.shift();

    // Let child rows handlers deal with the region change - without recursing
    // on nested group rows, we are handling them all here.
    $.each(rowObject.group, function() {
      var childRow = this;
      var childRowHandler = $(childRow).data('fieldUIRowHandler');
      $.extend(refreshRows, childRowHandler.regionChange(region, false));
    });
  }  
};
  
  
/**
 * Row handlers for the 'Manage display' screen.
 */
Drupal.fieldUIDisplayOverview = Drupal.fieldUIDisplayOverview || {};

Drupal.fieldUIDisplayOverview.group = function(row, data) {
  this.row = row;
  this.name = data.name;
  this.region = data.region;
  this.tableDrag = data.tableDrag;

  // Attach change listener to the 'group format' select.
  this.$formatSelect = $('select.field-group-type', row);
  this.$formatSelect.change(Drupal.fieldUIOverview.onChange);

  return this;
};

Drupal.fieldUIDisplayOverview.group.prototype = {
  getRegion: function () {
    return (this.$formatSelect.val() == 'hidden') ? 'hidden' : 'visible';
  },

  regionChange: function (region, recurse) {

    // Default recurse to true.
    recurse = (recurse == undefined) || recurse;

    // When triggered by a row drag, the 'format' select needs to be adjusted to
    // the new region.
    var currentValue = this.$formatSelect.val();
    switch (region) {
      case 'visible':
        if (currentValue == 'hidden') {
          // Restore the group format back to 'fieldset'.
          var value = 'fieldset';
        }
        break;

      default:
        var value = 'hidden';
        break;
    }
    if (value != undefined) {
      this.$formatSelect.val(value);
    }

    var refreshRows = {};
    refreshRows[this.name] = this.$formatSelect.get(0);

    if (recurse) {
      this.regionChangeFields(region, this, refreshRows);
    }

    return refreshRows;
  },

  regionChangeFields: function (region, element, refreshRows) {

    // Create a new tabledrag rowObject, that will compute the group's child
    // rows for us.
    var tableDrag = element.tableDrag;
    rowObject = new tableDrag.row(element.row, 'mouse', true);
    // Skip the main row, we handled it above.
    rowObject.group.shift();

    // Let child rows handlers deal with the region change - without recursing
    // on nested group rows, we are handling them all here.
    $.each(rowObject.group, function() {
      var childRow = this;
      var childRowHandler = $(childRow).data('fieldUIRowHandler');
      $.extend(refreshRows, childRowHandler.regionChange(region, false));
    });
    
  }
  
};

})(jQuery);;/**/
/**
 * @file
 * Javascript functionality for the Display Suite Extras administration UI.
 */

(function ($) {

Drupal.behaviors.DSExtrasSummaries = {
  attach: function (context) {

    $('#edit-additional-settings-fs1', context).drupalSetSummary(function (context) {
      var fieldtemplates = $('#edit-additional-settings-fs1-ds-extras-field-template', context);

      if (fieldtemplates.is(':checked')) {
        var fieldtemplate = $('#edit-additional-settings-fs1-ft-default option:selected').text();
        return Drupal.t('Enabled') + ': ' + Drupal.t(fieldtemplate);
      }

      return Drupal.t('Disabled');
    });

    $('#edit-additional-settings-fs2', context).drupalSetSummary(function (context) {
      var extra_fields = $('#edit-additional-settings-fs2-ds-extras-fields-extra', context);

      if (extra_fields.is(':checked')) {
        return Drupal.t('Enabled');
      }

      return Drupal.t('Disabled');
    });

    $('#edit-additional-settings-fs4', context).drupalSetSummary(function (context) {
      var vals = [];

      $('input:checked', context).parent().each(function () {
        vals.push(Drupal.checkPlain($.trim($('.option', this).text())));
      });

      if (vals.length > 0) {
        return vals.join(', ');
      }
      return Drupal.t('Disabled');
    });
  }
};

/**
 * Field template.
 */
Drupal.behaviors.settingsToggle = {
  attach: function (context) {

    // Bind on click.
    $('.field-formatter-settings-edit-form', context).once('ds-ft', function() {

      var fieldTemplate = $(this);

      // Bind on field template select button.
      fieldTemplate.find('.ds-extras-field-template').change(function() {
        ds_show_expert_settings(fieldTemplate);
      });

      ds_show_expert_settings(fieldTemplate);

    });

    // Show / hide settings on field template form.
    function ds_show_expert_settings(element, open) {
      field = element;
      ft = $('.ds-extras-field-template', field).val();

      if (ft == 'theme_ds_field_expert') {
        // Show second, third, fourth, fifth and sixth label.
        if ($('.lb .form-item:nth-child(1)', field).is(':visible')) {
          $('.lb .form-item:nth-child(2), .lb .form-item:nth-child(3), .lb .form-item:nth-child(4), .lb .form-item:nth-child(5), .lb .form-item:nth-child(6)', field).show();
        }
        // Remove margin from update button.
        $('.ft-update', field).css({'margin-top': '-10px'});
        // Show wrappers.
        $('.lbw, .ow, .fis, .fi', field).show();
        // Show prefix and suffix
        $('.field-prefix', field).show();
        $('.field-suffix', field).show();
      }
      else {
        // Hide second, third, fourth, fifth and sixth  label.
        $('.lb .form-item:nth-child(2), .lb .form-item:nth-child(3), .lb .form-item:nth-child(4), .lb .form-item:nth-child(5), .lb .form-item:nth-child(6)', field).hide();
        // Add margin on update button.
        $('.ft-update', field).css({'margin-top': '10px'});
        // Hide wrappers.
        $('.lbw, .ow, .fis, .fi', field).hide();
        // Hide prefix and suffix
        $('.field-prefix', field).hide();
        $('.field-suffix', field).hide();
      }

      // Colon.
      if (ft == 'theme_field' || ft == 'theme_ds_field_reset') {
        $('.colon-checkbox', field).parent().hide();
      }
      else if ($('.lb .form-item:nth-child(1)', field).is(':visible')) {
        $('.colon-checkbox', field).parent().show();
      }

      // CSS classes.
      if (ft != 'theme_ds_field_expert' && ft != 'theme_ds_field_reset') {
        $('.field-classes', field).show();
      }
      else {
        $('.field-classes', field).hide();
      }
    }

    $('.label-change').change(function() {
      var field = $(this).parents('tr');
      if ($('.field-template', field).length > 0) {
        ft = $('.ds-extras-field-template', field).val();
        if (ft == 'theme_field' || ft == 'theme_ds_field_reset') {
          $('.colon-checkbox', field).parent().hide();
        }
      }
    });
  }
};

})(jQuery);
;/**/
