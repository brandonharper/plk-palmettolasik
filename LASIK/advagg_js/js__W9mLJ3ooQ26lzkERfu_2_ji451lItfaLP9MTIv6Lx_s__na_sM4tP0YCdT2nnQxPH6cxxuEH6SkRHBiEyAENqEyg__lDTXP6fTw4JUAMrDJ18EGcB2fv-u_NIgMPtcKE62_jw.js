/* Source and licensing information for the line(s) below can be found at https://palmettolasik.com/sites/all/modules/wysiwyg/wysiwyg.js. */
(function($){if(typeof Drupal.wysiwygAttach!=='undefined'){return;}
var _fieldInfoStorage={};var _formatInfoStorage={};var _pluginInfoStorage={'global':{'drupal':{},'native':{}}};var _internalInstances={};var _initializedLibraries={};var _selectToField={};function getFieldInfo(fieldId){if(_fieldInfoStorage[fieldId]){return _fieldInfoStorage[fieldId];}
var baseFieldId=(fieldId.indexOf('--')===-1?fieldId:fieldId.substr(0,fieldId.indexOf('--')));if(_fieldInfoStorage[baseFieldId]){return _fieldInfoStorage[baseFieldId];}
throw new Error('Wysiwyg module has no information about field "'+fieldId+'"');}
function getFormatInfo(formatId){if(_formatInfoStorage[formatId]){return _formatInfoStorage[formatId];}
return{editor:'none',getPluginInfo:function(){return getPluginInfo(formatId);}};}
function getPluginInfo(formatId){var match,editor;if((match=formatId.match(/^global:(\w+)$/))){formatId='global';editor=match[1];}
if(!_pluginInfoStorage[formatId]){return{};}
if(formatId==='global'&&typeof editor!=='undefined'){return{'drupal':_pluginInfoStorage.global.drupal,'native':(_pluginInfoStorage.global['native'][editor])};}
return _pluginInfoStorage[formatId];}
Drupal.behaviors.attachWysiwyg={attach:function(context,settings){if(/KDE/.test(navigator.vendor)){return;}
var wysiwygs=$('.wysiwyg:input',context);if(!wysiwygs.length){return;}
updateInternalState(settings.wysiwyg,context);wysiwygs.once('wysiwyg',function(){var $this=$('#'+this.id,document);if(!$this.length){return;}
Drupal.wysiwygAttach(context,this.id);}).closest('form').submit(function(event){if(event.isDefaultPrevented()){return;}
var form=this;$('.wysiwyg:input',this).each(function(){Drupal.wysiwygDetach(form,this.id,'serialize');});});},detach:function(context,settings,trigger){var wysiwygs;if(trigger=='serialize'){wysiwygs=$('.wysiwyg-processed:input',context);}
else{wysiwygs=$('.wysiwyg:input',context).removeOnce('wysiwyg');}
wysiwygs.each(function(){Drupal.wysiwygDetach(context,this.id,trigger);if(trigger==='unload'){delete _internalInstances[this.id];delete Drupal.wysiwyg.instances[this.id];}});}};Drupal.wysiwygAttach=function(context,fieldId){var fieldInfo=getFieldInfo(fieldId),doSummary=(fieldInfo.summary&&(!fieldInfo.formats[fieldInfo.activeFormat]||!fieldInfo.formats[fieldInfo.activeFormat].skip_summary));detachFromField(fieldId,context,'unload');var wasSummary=!!_internalInstances[fieldInfo.summary];if(doSummary||wasSummary){detachFromField(fieldId,context,'unload',{summary:true});}
Drupal.wysiwyg.activeId=fieldId;Drupal.wysiwygAttachToggleLink(context,fieldId);attachToField(fieldId,context);if(doSummary||wasSummary){if($('#'+fieldInfo.summary).parents('.text-summary-wrapper').is(':visible')){attachToField(fieldId,context,{summary:true,forceDisabled:!doSummary});}
else{attachToField(fieldId,context,{summary:true,forceDisabled:true});$('#'+fieldId).parents('.text-format-wrapper').find('.link-edit-summary').closest('.field-edit-link').unbind('click.wysiwyg').bind('click.wysiwyg',function(){detachFromField(fieldId,context,'unload',{summary:true});attachToField(fieldId,context,{summary:true,forceDisabled:!doSummary});$(this).unbind('click.wysiwyg');});}}};function WysiwygInstance(internalInstance){this.field=internalInstance.field;this.editor=internalInstance.editor;this['status']=internalInstance['status'];this.format=internalInstance.format;this.resizable=internalInstance.resizable;this.insert=function(content){return internalInstance['status']?internalInstance.insert(content):Drupal.wysiwyg.editor.instance.none.insert.call(internalInstance,content);}
this.getContent=function(){return internalInstance['status']?internalInstance.getContent():Drupal.wysiwyg.editor.instance.none.getContent.call(internalInstance);}
this.setContent=function(content){return internalInstance['status']?internalInstance.setContent(content):Drupal.wysiwyg.editor.instance.none.setContent.call(internalInstance,content);}
this.isFullscreen=function(content){return internalInstance['status']&&$.isFunction(internalInstance.isFullscreen)?internalInstance.isFullscreen():false;}
this.openDialog=function(dialog,params){if($.isFunction(internalInstance.openDialog)){return internalInstance.openDialog(dialog,params)}}
this.closeDialog=function(dialog){if($.isFunction(internalInstance.closeDialog)){return internalInstance.closeDialog(dialog)}}}
function WysiwygInternalInstance(params){$.extend(true,this,Drupal.wysiwyg.editor.instance[params.editor]);$.extend(true,this,params);this.pluginInfo={'global':getPluginInfo('global:'+params.editor),'instances':getPluginInfo(params.format)};this.publicInstance=new WysiwygInstance(this);}
function updateInternalState(settings,context){var pluginData=settings.plugins;for(var plugin in pluginData.drupal){if(!(plugin in _pluginInfoStorage.global.drupal)){_pluginInfoStorage.global.drupal[plugin]=pluginData.drupal[plugin];}}
for(var editorId in pluginData['native']){for(var plugin in pluginData['native'][editorId]){_pluginInfoStorage.global['native'][editorId]=(_pluginInfoStorage.global['native'][editorId]||{});if(!(plugin in _pluginInfoStorage.global['native'][editorId])){_pluginInfoStorage.global['native'][editorId][plugin]=pluginData['native'][editorId][plugin];}}}
for(var fmatId in pluginData){if(fmatId.substr(0,6)!=='format'){continue;}
_pluginInfoStorage[fmatId]=(_pluginInfoStorage[fmatId]||{'drupal':{},'native':{}});for(var plugin in pluginData[fmatId].drupal){if(!(plugin in _pluginInfoStorage[fmatId].drupal)){_pluginInfoStorage[fmatId].drupal[plugin]=pluginData[fmatId].drupal[plugin];}}
for(var plugin in pluginData[fmatId]['native']){if(!(plugin in _pluginInfoStorage[fmatId]['native'])){_pluginInfoStorage[fmatId]['native'][plugin]=pluginData[fmatId]['native'][plugin];}}
delete pluginData[fmatId];}
for(var editor in settings.configs){if(!settings.configs.hasOwnProperty(editor)){continue;}
for(var format in settings.configs[editor]){if(_formatInfoStorage[format]||!settings.configs[editor].hasOwnProperty(format)){continue;}
_formatInfoStorage[format]={editor:editor,toggle:true,editorSettings:processObjectTypes(settings.configs[editor][format])};}
if(!_initializedLibraries[editor]&&typeof Drupal.wysiwyg.editor.init[editor]==='function'){Drupal.wysiwyg.editor.init[editor](jQuery.extend(true,{},settings.configs[editor]),getPluginInfo('global:'+editor));_initializedLibraries[editor]=true;}
else if(typeof Drupal.wysiwyg.editor.update[editor]==='function'){Drupal.wysiwyg.editor.update[editor](jQuery.extend(true,{},settings.configs[editor]),getPluginInfo('global:'+editor));}}
for(var triggerId in settings.triggers){var trigger=settings.triggers[triggerId];var fieldId=trigger.field;var baseFieldId=(fieldId.indexOf('--')===-1?fieldId:fieldId.substr(0,fieldId.indexOf('--')));var fieldInfo=null;if($('#'+triggerId,context).length===0){continue;}
if(!(fieldInfo=_fieldInfoStorage[baseFieldId])){fieldInfo=_fieldInfoStorage[baseFieldId]={formats:{},select:trigger.select,resizable:trigger.resizable,summary:trigger.summary,getFormatInfo:function(){if(this.select){this.activeFormat='format'+$('#'+this.select+':input').val();}
return getFormatInfo(this.activeFormat);}};}
for(var format in trigger){if(format.indexOf('format')!=0||fieldInfo.formats[format]){continue;}
fieldInfo.formats[format]={'enabled':trigger[format].status};if(!_formatInfoStorage[format]){_formatInfoStorage[format]={editor:trigger[format].editor,editorSettings:{},getPluginInfo:function(){return getPluginInfo(formatId);}};}
_formatInfoStorage[format].toggle=trigger[format].toggle;if(trigger[format].skip_summary){fieldInfo.formats[format].skip_summary=true;}}
var $selectbox=null;fieldInfo.summary=trigger.summary;if(trigger.select){_selectToField[trigger.select.replace(/--\d+$/,'')]=trigger.field;fieldInfo.select=trigger.select;$selectbox=$('#'+trigger.select+':input',context).filter('select');$selectbox.unbind('change.wysiwyg').bind('change.wysiwyg',formatChanged);}
fieldInfo.activeFormat='format'+($selectbox?$selectbox.val():trigger.activeFormat);fieldInfo.enabled=fieldInfo.formats[fieldInfo.activeFormat]&&fieldInfo.formats[fieldInfo.activeFormat].enabled;}}
function attachToField(mainFieldId,context,params){params=params||{};var fieldInfo=getFieldInfo(mainFieldId);var fieldId=(params.summary?fieldInfo.summary:mainFieldId);var formatInfo=fieldInfo.getFormatInfo();var enabled=(fieldInfo.enabled&&!params.forceDisabled);var editor=(enabled?formatInfo.editor:'none');var clonedSettings=(enabled?jQuery.extend(true,{},formatInfo.editorSettings):{});var stateParams={field:fieldId,editor:formatInfo.editor,'status':enabled,format:fieldInfo.activeFormat,resizable:fieldInfo.resizable};var internalInstance=new WysiwygInternalInstance(stateParams);_internalInstances[fieldId]=internalInstance;Drupal.wysiwyg.instances[fieldId]=internalInstance.publicInstance;Drupal.wysiwyg.editor.attach[editor].call(internalInstance,context,stateParams,clonedSettings);}
Drupal.wysiwygDetach=function(context,fieldId,trigger){var fieldInfo=getFieldInfo(fieldId),trigger=trigger||'unload';detachFromField(fieldId,context,trigger);if(trigger=='unload'){attachToField(fieldId,context,{forceDisabled:true});Drupal.wysiwygAttachToggleLink(context,fieldId);}
if(fieldInfo.summary&&_internalInstances[fieldInfo.summary]){$('#'+fieldId).parents('.text-format-wrapper').find('.link-edit-summary').unbind('click.wysiwyg');detachFromField(fieldId,context,trigger,{summary:true});if(trigger=='unload'){attachToField(fieldId,context,{summary:true});}}};function detachFromField(mainFieldId,context,trigger,params){params=params||{};var fieldInfo=getFieldInfo(mainFieldId);var fieldId=(params.summary?fieldInfo.summary:mainFieldId);var enabled=false;var editor='none';if(_internalInstances[fieldId]){enabled=_internalInstances[fieldId]['status'];editor=(enabled?_internalInstances[fieldId].editor:'none');}
var stateParams={field:fieldId,'status':enabled,editor:fieldInfo.editor,format:fieldInfo.activeFormat,resizable:fieldInfo.resizable};if(jQuery.isFunction(Drupal.wysiwyg.editor.detach[editor])){Drupal.wysiwyg.editor.detach[editor].call(_internalInstances[fieldId],context,stateParams,trigger);}
if(trigger=='unload'){delete Drupal.wysiwyg.instances[fieldId];delete _internalInstances[fieldId];}}
Drupal.wysiwygAttachToggleLink=function(context,fieldId){var fieldInfo=getFieldInfo(fieldId),editor=fieldInfo.getFormatInfo().editor;if(!fieldInfo.getFormatInfo().toggle){$('#wysiwyg-toggle-'+fieldId).hide();return;}
if(!$('#wysiwyg-toggle-'+fieldId,context).length){var text=document.createTextNode(fieldInfo.enabled?Drupal.settings.wysiwyg.disable:Drupal.settings.wysiwyg.enable),a=document.createElement('a'),div=document.createElement('div');$(a).attr({id:'wysiwyg-toggle-'+fieldId,href:'javascript:void(0);'}).append(text);$(div).addClass('wysiwyg-toggle-wrapper').append(a);if($('#'+fieldInfo.select).closest('.fieldset-wrapper').prepend(div).length==0){$('#'+fieldId).after(div);};}
$('#wysiwyg-toggle-'+fieldId,context).html(fieldInfo.enabled?Drupal.settings.wysiwyg.disable:Drupal.settings.wysiwyg.enable).show().unbind('click.wysiwyg').bind('click.wysiwyg',{'fieldId':fieldId,'context':context},Drupal.wysiwyg.toggleWysiwyg);if(editor=='none'){$('#wysiwyg-toggle-'+fieldId).hide();}};Drupal.wysiwyg.toggleWysiwyg=function(event){var context=event.data.context,fieldId=event.data.fieldId,fieldInfo=getFieldInfo(fieldId);if(fieldInfo.enabled){fieldInfo.enabled=false;Drupal.wysiwygDetach(context,fieldId,'unload');}
else{fieldInfo.enabled=true;Drupal.wysiwygAttach(context,fieldId);}
fieldInfo.formats[fieldInfo.activeFormat].enabled=fieldInfo.enabled;}
function formatChanged(event){var fieldId=_selectToField[this.id.replace(/--\d+$/,'')];var context=$(this).closest('form');var newFormat='format'+$(this).val();var currentField=getFieldInfo(fieldId);if(newFormat===currentField.activeFormat){return;}
if(currentField.formats[currentField.activeFormat]){currentField.formats[currentField.activeFormat].enabled=currentField.enabled;}
currentField.activeFormat=newFormat;if(currentField.formats[currentField.activeFormat]){currentField.enabled=currentField.formats[currentField.activeFormat].enabled;}
else{currentField.enabled=false;}
Drupal.wysiwygAttach(context,fieldId);}
function processObjectTypes(json){var out=null;if(typeof json!='object'){return json;}
out=new json.constructor();if(json.drupalWysiwygType){switch(json.drupalWysiwygType){case'callback':out=callbackWrapper(json.name,json.context);break;case'regexp':out=new RegExp(json.regexp,json.modifiers?json.modifiers:undefined);break;default:out.drupalWysiwygType=json.drupalWysiwygType;}}
else{for(var i in json){if(json.hasOwnProperty(i)&&json[i]&&typeof json[i]=='object'){out[i]=processObjectTypes(json[i]);}
else{out[i]=json[i];}}}
return out;}
function callbackWrapper(name,context){var namespaces=name.split('.'),func=namespaces.pop(),obj=window;for(var i=0;obj&&i<namespaces.length;i++){obj=obj[namespaces[i]];}
if(!obj){throw"Wysiwyg: Unable to locate callback "+namespaces.join('.')+"."+func+"()";}
if(!context){context=obj;}
else if(typeof context=='string'){namespaces=context.split('.');context=window;for(i=0;context&&i<namespaces.length;i++){context=context[namespaces[i]];}
if(!context){throw"Wysiwyg: Unable to locate context object "+namespaces.join('.');}}
if(typeof obj[func]!='function'){throw"Wysiwyg: "+func+" is not a callback function";}
return function(){return obj[func].apply(context,arguments);}}
var oldBeforeSerialize=(Drupal.ajax?Drupal.ajax.prototype.beforeSerialize:false);if(oldBeforeSerialize){Drupal.ajax.prototype.beforeSerialize=function(element,options){var ret=oldBeforeSerialize.call(this,element,options);var excludeSelectors=[];$.each(Drupal.wysiwyg.excludeIdSelectors,function(){if($.isArray(this)){excludeSelectors=excludeSelectors.concat(this);}});if(excludeSelectors.length>0){var ajaxHtmlIdsArray=options.data['ajax_html_ids[]'];if(!ajaxHtmlIdsArray||ajaxHtmlIdsArray.length===0){return ret;}
options.data['ajax_html_ids[]']=[];$('[id]:not('+excludeSelectors.join(',')+')').each(function(){if($.inArray(this.id,ajaxHtmlIdsArray)!==-1){options.data['ajax_html_ids[]'].push(this.id);}});}
return ret;};}
$(document).unbind('CToolsDetachBehaviors.wysiwyg').bind('CToolsDetachBehaviors.wysiwyg',function(event,context){$('.wysiwyg:input',context).removeOnce('wysiwyg').each(function(){Drupal.wysiwygDetach(context,this.id,'unload');delete Drupal.wysiwyg.instances[this.id];delete _internalInstances[this.id];var baseFieldId=(this.id.indexOf('--')===-1?this.id:this.id.substr(0,this.id.indexOf('--')));delete _fieldInfoStorage[baseFieldId];});});})(jQuery);;
/* Source and licensing information for the above line(s) can be found at https://palmettolasik.com/sites/all/modules/wysiwyg/wysiwyg.js. */
