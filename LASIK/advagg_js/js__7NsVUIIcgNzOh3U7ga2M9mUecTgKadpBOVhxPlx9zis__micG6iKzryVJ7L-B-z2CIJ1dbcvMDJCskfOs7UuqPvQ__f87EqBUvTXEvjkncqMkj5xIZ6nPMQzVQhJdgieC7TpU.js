(function ($) {

Drupal.behaviors.textarea = {
  attach: function (context, settings) {
    $('.form-textarea-wrapper.resizable', context).once('textarea', function () {
      var staticOffset = null;
      var textarea = $(this).addClass('resizable-textarea').find('textarea');
      var grippie = $('<div class="grippie"></div>').mousedown(startDrag);

      grippie.insertAfter(textarea);

      function startDrag(e) {
        staticOffset = textarea.height() - e.pageY;
        textarea.css('opacity', 0.25);
        $(document).mousemove(performDrag).mouseup(endDrag);
        return false;
      }

      function performDrag(e) {
        textarea.height(Math.max(32, staticOffset + e.pageY) + 'px');
        return false;
      }

      function endDrag(e) {
        $(document).unbind('mousemove', performDrag).unbind('mouseup', endDrag);
        textarea.css('opacity', 1);
      }
    });
  }
};

})(jQuery);
;/**/
/**
 * @file
 * Provides dependent visibility for form items in CTools' ajax forms.
 *
 * To your $form item definition add:
 * - '#process' => array('ctools_process_dependency'),
 * - '#dependency' => array('id-of-form-item' => array(list, of, values, that,
 *   make, this, item, show),
 *
 * Special considerations:
 * - Radios are harder. Because Drupal doesn't give radio groups individual IDs,
 *   use 'radio:name-of-radio'.
 *
 * - Checkboxes don't have their own id, so you need to add one in a div
 *   around the checkboxes via #prefix and #suffix. You actually need to add TWO
 *   divs because it's the parent that gets hidden. Also be sure to retain the
 *   'expand_checkboxes' in the #process array, because the CTools process will
 *   override it.
 */

(function ($) {
  Drupal.CTools = Drupal.CTools || {};
  Drupal.CTools.dependent = {};

  Drupal.CTools.dependent.bindings = {};
  Drupal.CTools.dependent.activeBindings = {};
  Drupal.CTools.dependent.activeTriggers = [];

  Drupal.CTools.dependent.inArray = function(array, search_term) {
    var i = array.length;
    while (i--) {
      if (array[i] == search_term) {
         return true;
      }
    }
    return false;
  }


  Drupal.CTools.dependent.autoAttach = function() {
    // Clear active bindings and triggers.
    for (i in Drupal.CTools.dependent.activeTriggers) {
      $(Drupal.CTools.dependent.activeTriggers[i]).unbind('change.ctools-dependent');
    }
    Drupal.CTools.dependent.activeTriggers = [];
    Drupal.CTools.dependent.activeBindings = {};
    Drupal.CTools.dependent.bindings = {};

    if (!Drupal.settings.CTools) {
      return;
    }

    // Iterate through all relationships
    for (id in Drupal.settings.CTools.dependent) {
      // Test to make sure the id even exists; this helps clean up multiple
      // AJAX calls with multiple forms.

      // Drupal.CTools.dependent.activeBindings[id] is a boolean,
      // whether the binding is active or not.  Defaults to no.
      Drupal.CTools.dependent.activeBindings[id] = 0;
      // Iterate through all possible values
      for(bind_id in Drupal.settings.CTools.dependent[id].values) {
        // This creates a backward relationship.  The bind_id is the ID
        // of the element which needs to change in order for the id to hide or become shown.
        // The id is the ID of the item which will be conditionally hidden or shown.
        // Here we're setting the bindings for the bind
        // id to be an empty array if it doesn't already have bindings to it
        if (!Drupal.CTools.dependent.bindings[bind_id]) {
          Drupal.CTools.dependent.bindings[bind_id] = [];
        }
        // Add this ID
        Drupal.CTools.dependent.bindings[bind_id].push(id);
        // Big long if statement.
        // Drupal.settings.CTools.dependent[id].values[bind_id] holds the possible values

        if (bind_id.substring(0, 6) == 'radio:') {
          var trigger_id = "input[name='" + bind_id.substring(6) + "']";
        }
        else {
          var trigger_id = '#' + bind_id;
        }

        Drupal.CTools.dependent.activeTriggers.push(trigger_id);

        if ($(trigger_id).attr('type') == 'checkbox') {
          $(trigger_id).siblings('label').addClass('hidden-options');
        }

        var getValue = function(item, trigger) {
          if ($(trigger).size() == 0) {
            return null;
          }

          if (item.substring(0, 6) == 'radio:') {
            var val = $(trigger + ':checked').val();
          }
          else {
            switch ($(trigger).attr('type')) {
              case 'checkbox':
                var val = $(trigger).attr('checked') ? true : false;

                if (val) {
                  $(trigger).siblings('label').removeClass('hidden-options').addClass('expanded-options');
                }
                else {
                  $(trigger).siblings('label').removeClass('expanded-options').addClass('hidden-options');
                }

                break;
              default:
                var val = $(trigger).val();
            }
          }
          return val;
        }

        var setChangeTrigger = function(trigger_id, bind_id) {
          // Triggered when change() is clicked.
          var changeTrigger = function() {
            var val = getValue(bind_id, trigger_id);

            if (val == null) {
              return;
            }

            for (i in Drupal.CTools.dependent.bindings[bind_id]) {
              var id = Drupal.CTools.dependent.bindings[bind_id][i];
              // Fix numerous errors
              if (typeof id != 'string') {
                continue;
              }

              // This bit had to be rewritten a bit because two properties on the
              // same set caused the counter to go up and up and up.
              if (!Drupal.CTools.dependent.activeBindings[id]) {
                Drupal.CTools.dependent.activeBindings[id] = {};
              }

              if (val != null && Drupal.CTools.dependent.inArray(Drupal.settings.CTools.dependent[id].values[bind_id], val)) {
                Drupal.CTools.dependent.activeBindings[id][bind_id] = 'bind';
              }
              else {
                delete Drupal.CTools.dependent.activeBindings[id][bind_id];
              }

              var len = 0;
              for (i in Drupal.CTools.dependent.activeBindings[id]) {
                len++;
              }

              var object = $('#' + id + '-wrapper');
              if (!object.size()) {
                // Some elements can't use the parent() method or they can
                // damage things. They are guaranteed to have wrappers but
                // only if dependent.inc provided them. This check prevents
                // problems when multiple AJAX calls cause settings to build
                // up.
                var $original = $('#' + id);
                if ($original.is('fieldset') || $original.is('textarea')) {
                  continue;
                }

                object = $('#' + id).parent();
              }

              if (Drupal.settings.CTools.dependent[id].type == 'disable') {
                if (Drupal.settings.CTools.dependent[id].num <= len) {
                  // Show if the element if criteria is matched
                  object.attr('disabled', false);
                  object.addClass('dependent-options');
                  object.children().attr('disabled', false);
                }
                else {
                  // Otherwise hide. Use css rather than hide() because hide()
                  // does not work if the item is already hidden, for example,
                  // in a collapsed fieldset.
                  object.attr('disabled', true);
                  object.children().attr('disabled', true);
                }
              }
              else {
                if (Drupal.settings.CTools.dependent[id].num <= len) {
                  // Show if the element if criteria is matched
                  object.show(0);
                  object.addClass('dependent-options');
                }
                else {
                  // Otherwise hide. Use css rather than hide() because hide()
                  // does not work if the item is already hidden, for example,
                  // in a collapsed fieldset.
                  object.css('display', 'none');
                }
              }
            }
          }

          $(trigger_id).bind('change.ctools-dependent', function() {
            // Trigger the internal change function
            // the attr('id') is used because closures are more confusing
            changeTrigger(trigger_id, bind_id);
          });
          // Trigger initial reaction
          changeTrigger(trigger_id, bind_id);
        }
        setChangeTrigger(trigger_id, bind_id);
      }
    }
  }

  Drupal.behaviors.CToolsDependent = {
    attach: function (context) {
      Drupal.CTools.dependent.autoAttach();

      // Really large sets of fields are too slow with the above method, so this
      // is a sort of hacked one that's faster but much less flexible.
      $("select.ctools-master-dependent")
        .once('ctools-dependent')
        .bind('change.ctools-dependent', function() {
          var val = $(this).val();
          if (val == 'all') {
            $('.ctools-dependent-all').show(0);
          }
          else {
            $('.ctools-dependent-all').hide(0);
            $('.ctools-dependent-' + val).show(0);
          }
        })
        .trigger('change.ctools-dependent');
    }
  }
})(jQuery);
;/**/
(function($) {

/**
 * Context plugin form.
 */
function DrupalContextPlugins(form) {
  this.form = form;

  // Sync the form selector and state field with the list of plugins currently enabled.
  this.setState = function() {
    var state = [];
    $('.context-plugin-list > li', this.form).each(function() {
      var plugin = $(this).attr('class').split('context-plugin-')[1].split(' ')[0];
      if ($(this).is('.disabled')) {
        $('.context-plugin-selector select option[value='+plugin+']', this.form).show();
      }
      else {
        state.push(plugin);
        $('.context-plugin-selector select option[value='+plugin+']', this.form).hide();
      }
    });
    // Set the hidden plugin list state.
    $('.context-plugin-selector input.context-plugins-state', this.form).val(state.join(','));

    // Reset the selector.
    $('.context-plugin-selector select', this.form).val(0);
    return this;
  };

  // Add a plugin to the list.
  this.addPlugin = function(plugin) {
    $('.context-plugin-list > li.context-plugin-'+plugin, this.form).removeClass('disabled');
    this.showForm(plugin).setState();
    return this;
  };

  // Remove a plugin from the list.
  this.removePlugin = function(plugin) {
    $('.context-plugin-list > li.context-plugin-'+plugin, this.form).addClass('disabled');
    this.hideForm(plugin).setState();
    return this;
  };

  // Show a plugin form.
  this.showForm = function(plugin) {
    $('.context-plugin-forms > .context-plugin-form.active-form', this.form).removeClass('active-form');
    $('.context-plugin-forms > .context-plugin-form-'+plugin, this.form).addClass('active-form');
    $('.context-plugin-list > li > a').removeClass('active-form');
    $('.context-plugin-list > li.context-plugin-'+plugin+' > a').addClass('active-form');
    return this;
  };

  // Show a plugin form.
  this.hideForm = function(plugin) {
    $('.context-plugin-forms > .context-plugin-form-'+plugin, this.form).removeClass('active-form');
    $('.context-plugin-list > li.context-plugin-'+plugin+' > a').removeClass('active-form');
    return this;
  };

  // Select handler.
  $('.context-plugin-selector select', this.form).change(function() {
    var plugins = $(this).parents('div.context-plugins').data('contextPlugins');
    if (plugins) {
      var plugin = $(this).val();
      plugins.addPlugin(plugin);
    }
  });

  // Show form handler.
  $('.context-plugin-list > li > a', this.form).click(function() {
    var plugins = $(this).parents('div.context-plugins').data('contextPlugins');
    if (plugins) {
      var plugin = $(this).attr('href').split('#context-plugin-form-')[1];
      plugins.showForm(plugin);
    }
    return false;
  });

  // Remove handler.
  $('.context-plugin-list span.remove', this.form).click(function() {
    var plugins = $(this).parents('div.context-plugins').data('contextPlugins');
    if (plugins) {
      var plugin = $(this).parent().attr('href').split('#context-plugin-form-')[1];
      plugins.removePlugin(plugin);
    }
    return false;
  });

  // Set the plugin states.
  this.setState();
}

Drupal.behaviors.context_ui = { attach: function(context) {
  // Initialize context plugin form.
  $('form div.context-plugins:not(.context-ui-processed)').each(function() {
    $(this).addClass('context-ui-processed');
    $(this).data('contextPlugins', new DrupalContextPlugins($(this)));
  });

  // Initialize context editor.
  if ($().pageEditor) {
    $('form.context-editor:not(.context-ui-processed)')
      .addClass('context-ui-processed')
      .pageEditor()
      .each(function() {
        var editor = $(this);
        var defaultContext = $('li.context-editable', this).attr('id').split('context-editable-trigger-')[1];
        $(this).data('defaultContext', defaultContext);

        // Attach start/end handlers to editable contexts.
        $('li.context-editable a.edit', editor).click(function() {
          var trigger = $(this).parents('li.context-editable').addClass('context-editing');
          var context = trigger.attr('id').split('context-editable-trigger-')[1];
          editor.pageEditor('start', context);
          return false;
        });
        $('li.context-editable a.done', editor).click(function() {
          editor.pageEditor('end');
          return false;
        });
        $(editor).submit(function() {
          if (editor.pageEditor('isEditing')) {
            editor.pageEditor('end');
          }
        });

        // Handler for start event.
        editor.bind('start.pageEditor', function(event, context) {
          // Fallback to first context if param is empty.
          if (!context) {
            context = $(this).data('defaultContext');
            $('li#context-editable-trigger-'+context, this).addClass('context-editing');
          }
          $(document.body).addClass('context-editing');
          $('#context-editable-'+context, this).show();
        });

        // Handler for end event.
        editor.bind('end.pageEditor', function(event, context) {
          $(document.body).removeClass('context-editing');
          $('div.contexts div.context-editable', this).hide();
          $('li.context-editable').removeClass('context-editing');
          $('form.context-editor').addClass('edited');
        });
      });
  }
}};
})(jQuery);
;/**/

/**
 * Generic pageEditor plugin. Allows an editor DOM object to trigger
 * init, start, and end events. Implementors can check whether the
 * editor is currently editing and bind handlers for the events triggered
 * by the editor.
 */
(function($) {
  $.fn.pageEditor = function(method, data) {
    this.each(function() {
      switch (method) {
        case 'isEditing':
          return this.editing;
        case 'start':
          if (!this.inited) {
            this.inited = true;
            $(this).trigger('init.pageEditor', data);
          }
          this.editing = true;
          $(this).trigger('start.pageEditor', data);
          break;
        case 'end':
          if (!this.inited) {
            this.inited = true;
            $(this).trigger('init.pageEditor', data);
          }
          this.editing = false;
          $(this).trigger('end.pageEditor', data);
          break;
        default:
          this.inited = false;
          this.editing = false;
          break;
      }
    });
    return this;
  };
})(jQuery);
;/**/
