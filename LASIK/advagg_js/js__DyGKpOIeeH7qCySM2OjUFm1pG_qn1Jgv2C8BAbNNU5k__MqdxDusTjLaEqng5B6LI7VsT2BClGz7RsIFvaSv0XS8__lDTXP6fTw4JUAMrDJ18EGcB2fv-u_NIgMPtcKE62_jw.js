/* Source and licensing information for the line(s) below can be found at https://palmettolasik.com/sites/all/modules/drupalexp/modules/dexp_gmap/js/jquery.gomap-1.3.2.js. */
(function($){var geocoder=new google.maps.Geocoder();function MyOverlay(map){this.setMap(map);};MyOverlay.prototype=new google.maps.OverlayView();MyOverlay.prototype.onAdd=function(){};MyOverlay.prototype.onRemove=function(){};MyOverlay.prototype.draw=function(){};$.goMap={};$.fn.goMap=function(options){return this.each(function(){var goMap=$(this).data('goMap');if(!goMap){var goMapBase=$.extend(true,{},$.goMapBase);$(this).data('goMap',goMapBase.init(this,options));$.goMap=goMapBase;}
else{$.goMap=goMap;}});};$.goMapBase={defaults:{address:'',latitude:56.9,longitude:24.1,zoom:4,delay:200,hideByClick:true,oneInfoWindow:true,prefixId:'gomarker',polyId:'gopoly',groupId:'gogroup',navigationControl:true,navigationControlOptions:{position:'TOP_LEFT',style:'DEFAULT'},mapTypeControl:true,mapTypeControlOptions:{position:'TOP_RIGHT',style:'DEFAULT'},scaleControl:false,scrollwheel:true,directions:false,directionsResult:null,disableDoubleClickZoom:false,streetViewControl:false,markers:[],overlays:[],polyline:{color:'#FF0000',opacity:1.0,weight:2},polygon:{color:'#FF0000',opacity:1.0,weight:2,fillColor:'#FF0000',fillOpacity:0.2},circle:{color:'#FF0000',opacity:1.0,weight:2,fillColor:'#FF0000',fillOpacity:0.2},rectangle:{color:'#FF0000',opacity:1.0,weight:2,fillColor:'#FF0000',fillOpacity:0.2},maptype:'HYBRID',html_prepend:'<div class=gomapMarker>',html_append:'</div>',addMarker:false},map:null,count:0,markers:[],polylines:[],polygons:[],circles:[],rectangles:[],tmpMarkers:[],geoMarkers:[],lockGeocode:false,bounds:null,overlays:null,overlay:null,mapId:null,plId:null,pgId:null,cId:null,rId:null,opts:null,centerLatLng:null,init:function(el,options){var opts=$.extend(true,{},$.goMapBase.defaults,options);this.mapId=$(el);this.opts=opts;if(opts.address)
this.geocode({address:opts.address,center:true});else if($.isArray(opts.markers)&&opts.markers.length>0){if(opts.markers[0].address)
this.geocode({address:opts.markers[0].address,center:true});else
this.centerLatLng=new google.maps.LatLng(opts.markers[0].latitude,opts.markers[0].longitude);}
else
this.centerLatLng=new google.maps.LatLng(opts.latitude,opts.longitude);var myOptions={center:this.centerLatLng,disableDoubleClickZoom:opts.disableDoubleClickZoom,mapTypeControl:opts.mapTypeControl,streetViewControl:opts.streetViewControl,mapTypeControlOptions:{position:google.maps.ControlPosition[opts.mapTypeControlOptions.position.toUpperCase()],style:google.maps.MapTypeControlStyle[opts.mapTypeControlOptions.style.toUpperCase()]},mapTypeId:google.maps.MapTypeId[opts.maptype.toUpperCase()],navigationControl:opts.navigationControl,navigationControlOptions:{position:google.maps.ControlPosition[opts.navigationControlOptions.position.toUpperCase()],style:google.maps.NavigationControlStyle[opts.navigationControlOptions.style.toUpperCase()]},scaleControl:opts.scaleControl,scrollwheel:opts.scrollwheel,zoom:opts.zoom};this.map=new google.maps.Map(el,myOptions);this.overlay=new MyOverlay(this.map);this.overlays={polyline:{id:'plId',array:'polylines',create:'createPolyline'},polygon:{id:'pgId',array:'polygons',create:'createPolygon'},circle:{id:'cId',array:'circles',create:'createCircle'},rectangle:{id:'rId',array:'rectangles',create:'createRectangle'}};this.plId=$('<div style="display:none;"/>').appendTo(this.mapId);this.pgId=$('<div style="display:none;"/>').appendTo(this.mapId);this.cId=$('<div style="display:none;"/>').appendTo(this.mapId);this.rId=$('<div style="display:none;"/>').appendTo(this.mapId);for(var j=0,l=opts.markers.length;j<l;j++)
this.createMarker(opts.markers[j]);for(var j=0,l=opts.overlays.length;j<l;j++)
this[this.overlays[opts.overlays[j].type].create](opts.overlays[j]);var goMap=this;if(opts.addMarker==true||opts.addMarker=='multi'){google.maps.event.addListener(goMap.map,'click',function(event){var options={position:event.latLng,draggable:true};var marker=goMap.createMarker(options);google.maps.event.addListener(marker,'dblclick',function(event){marker.setMap(null);goMap.removeMarker(marker.id);});});}
else if(opts.addMarker=='single'){google.maps.event.addListener(goMap.map,'click',function(event){if(!goMap.singleMarker){var options={position:event.latLng,draggable:true};var marker=goMap.createMarker(options);goMap.singleMarker=true;google.maps.event.addListener(marker,'dblclick',function(event){marker.setMap(null);goMap.removeMarker(marker.id);goMap.singleMarker=false;});}});}
delete opts.markers;delete opts.overlays;return this;},ready:function(f){google.maps.event.addListenerOnce(this.map,'bounds_changed',function(){return f();});},geocode:function(address,options){var goMap=this;setTimeout(function(){geocoder.geocode({'address':address.address},function(results,status){if(status==google.maps.GeocoderStatus.OK&&address.center)
goMap.map.setCenter(results[0].geometry.location);if(status==google.maps.GeocoderStatus.OK&&options&&options.markerId)
options.markerId.setPosition(results[0].geometry.location);else if(status==google.maps.GeocoderStatus.OK&&options){if(goMap.lockGeocode){goMap.lockGeocode=false;options.position=results[0].geometry.location;options.geocode=true;goMap.createMarker(options);}}
else if(status==google.maps.GeocoderStatus.OVER_QUERY_LIMIT){goMap.geocode(address,options);}});},this.opts.delay);},geoMarker:function(){if(this.geoMarkers.length>0&&!this.lockGeocode){this.lockGeocode=true;var current=this.geoMarkers.splice(0,1);this.geocode({address:current[0].address},current[0]);}
else if(this.lockGeocode){var goMap=this;setTimeout(function(){goMap.geoMarker();},this.opts.delay);}},setMap:function(options){delete options.mapTypeId;if(options.address){this.geocode({address:options.address,center:true});delete options.address;}
else if(options.latitude&&options.longitude){options.center=new google.maps.LatLng(options.latitude,options.longitude);delete options.longitude;delete options.latitude;}
if(options.mapTypeControlOptions&&options.mapTypeControlOptions.position)
options.mapTypeControlOptions.position=google.maps.ControlPosition[options.mapTypeControlOptions.position.toUpperCase()];if(options.mapTypeControlOptions&&options.mapTypeControlOptions.style)
options.mapTypeControlOptions.style=google.maps.MapTypeControlStyle[options.mapTypeControlOptions.style.toUpperCase()];if(options.navigationControlOptions&&options.navigationControlOptions.position)
options.navigationControlOptions.position=google.maps.ControlPosition[options.navigationControlOptions.position.toUpperCase()];if(options.navigationControlOptions&&options.navigationControlOptions.style)
options.navigationControlOptions.style=google.maps.NavigationControlStyle[options.navigationControlOptions.style.toUpperCase()];this.map.setOptions(options);},getMap:function(){return this.map;},createListener:function(type,event,data){var target;if(typeof type!='object')
type={type:type};if(type.type=='map')
target=this.map;else if(type.type=='marker'&&type.marker)
target=$(this.mapId).data(type.marker);else if(type.type=='info'&&type.marker)
target=$(this.mapId).data(type.marker+'info');if(target)
return google.maps.event.addListener(target,event,data);else if((type.type=='marker'||type.type=='info')&&this.getMarkerCount()!=this.getTmpMarkerCount())
var goMap=this;setTimeout(function(){goMap.createListener(type,event,data);},this.opts.delay);},removeListener:function(listener){google.maps.event.removeListener(listener);},setInfoWindow:function(marker,html){var goMap=this;html.content=goMap.opts.html_prepend+html.content+goMap.opts.html_append;var infowindow=new google.maps.InfoWindow(html);infowindow.show=false;$(goMap.mapId).data(marker.id+'info',infowindow);if(html.popup){goMap.openWindow(infowindow,marker,html);infowindow.show=true;}
google.maps.event.addListener(marker,'click',function(){if(infowindow.show&&goMap.opts.hideByClick){infowindow.close();infowindow.show=false;}
else{goMap.openWindow(infowindow,marker,html);infowindow.show=true;}});},openWindow:function(infowindow,marker,html){if(this.opts.oneInfoWindow)
this.clearInfo();if(html.ajax){infowindow.open(this.map,marker);$.ajax({url:html.ajax,success:function(html){infowindow.setContent(html);}});}
else if(html.id){infowindow.setContent($(html.id).html());infowindow.open(this.map,marker);}
else
infowindow.open(this.map,marker);},setInfo:function(id,text){var info=$(this.mapId).data(id+'info');if(typeof text=='object')
info.setOptions(text);else
info.setContent(text);},getInfo:function(id,hideDiv){var info=$(this.mapId).data(id+'info').getContent();if(hideDiv)
return $(info).html();else
return info;},clearInfo:function(){for(var i=0,l=this.markers.length;i<l;i++){var info=$(this.mapId).data(this.markers[i]+'info');if(info){info.close();info.show=false;}}},fitBounds:function(type,markers){var goMap=this;if(this.getMarkerCount()!=this.getTmpMarkerCount())
setTimeout(function(){goMap.fitBounds(type,markers);},this.opts.delay);else{this.bounds=new google.maps.LatLngBounds();if(!type||(type&&type=='all')){for(var i=0,l=this.markers.length;i<l;i++){this.bounds.extend($(this.mapId).data(this.markers[i]).position);}}
else if(type&&type=='visible'){for(var i=0,l=this.markers.length;i<l;i++){if(this.getVisibleMarker(this.markers[i]))
this.bounds.extend($(this.mapId).data(this.markers[i]).position);}}
else if(type&&type=='markers'&&$.isArray(markers)){for(var i=0,l=markers.length;i<l;i++){this.bounds.extend($(this.mapId).data(markers[i]).position);}}
this.map.fitBounds(this.bounds);}},getBounds:function(){return this.map.getBounds();},createPolyline:function(poly){poly.type='polyline';return this.createOverlay(poly);},createPolygon:function(poly){poly.type='polygon';return this.createOverlay(poly);},createCircle:function(poly){poly.type='circle';return this.createOverlay(poly);},createRectangle:function(poly){poly.type='rectangle';return this.createOverlay(poly);},createOverlay:function(poly){var overlay=[];if(!poly.id){this.count++;poly.id=this.opts.polyId+this.count;}
switch(poly.type){case'polyline':if(poly.coords.length>0){for(var j=0,l=poly.coords.length;j<l;j++)
overlay.push(new google.maps.LatLng(poly.coords[j].latitude,poly.coords[j].longitude));overlay=new google.maps.Polyline({map:this.map,path:overlay,strokeColor:poly.color?poly.color:this.opts.polyline.color,strokeOpacity:poly.opacity?poly.opacity:this.opts.polyline.opacity,strokeWeight:poly.weight?poly.weight:this.opts.polyline.weight});}
else
return false;break;case'polygon':if(poly.coords.length>0){for(var j=0,l=poly.coords.length;j<l;j++)
overlay.push(new google.maps.LatLng(poly.coords[j].latitude,poly.coords[j].longitude));overlay=new google.maps.Polygon({map:this.map,path:overlay,strokeColor:poly.color?poly.color:this.opts.polygon.color,strokeOpacity:poly.opacity?poly.opacity:this.opts.polygon.opacity,strokeWeight:poly.weight?poly.weight:this.opts.polygon.weight,fillColor:poly.fillColor?poly.fillColor:this.opts.polygon.fillColor,fillOpacity:poly.fillOpacity?poly.fillOpacity:this.opts.polygon.fillOpacity});}
else
return false;break;case'circle':overlay=new google.maps.Circle({map:this.map,center:new google.maps.LatLng(poly.latitude,poly.longitude),radius:poly.radius,strokeColor:poly.color?poly.color:this.opts.circle.color,strokeOpacity:poly.opacity?poly.opacity:this.opts.circle.opacity,strokeWeight:poly.weight?poly.weight:this.opts.circle.weight,fillColor:poly.fillColor?poly.fillColor:this.opts.circle.fillColor,fillOpacity:poly.fillOpacity?poly.fillOpacity:this.opts.circle.fillOpacity});break;case'rectangle':overlay=new google.maps.Rectangle({map:this.map,bounds:new google.maps.LatLngBounds(new google.maps.LatLng(poly.sw.latitude,poly.sw.longitude),new google.maps.LatLng(poly.ne.latitude,poly.ne.longitude)),strokeColor:poly.color?poly.color:this.opts.circle.color,strokeOpacity:poly.opacity?poly.opacity:this.opts.circle.opacity,strokeWeight:poly.weight?poly.weight:this.opts.circle.weight,fillColor:poly.fillColor?poly.fillColor:this.opts.circle.fillColor,fillOpacity:poly.fillOpacity?poly.fillOpacity:this.opts.circle.fillOpacity});break;default:return false;break;}
this.addOverlay(poly,overlay);return overlay;},addOverlay:function(poly,overlay){$(this[this.overlays[poly.type].id]).data(poly.id,overlay);this[this.overlays[poly.type].array].push(poly.id);},setOverlay:function(type,overlay,options){overlay=$(this[this.overlays[type].id]).data(overlay);if(options.coords&&options.coords.length>0){var array=[];for(var j=0,l=options.coords.length;j<l;j++)
array.push(new google.maps.LatLng(options.coords[j].latitude,options.coords[j].longitude));options.path=array;delete options.coords;}
else if(options.ne&&options.sw){options.bounds=new google.maps.LatLngBounds(new google.maps.LatLng(options.sw.latitude,options.sw.longitude),new google.maps.LatLng(options.ne.latitude,options.ne.longitude));delete options.ne;delete options.sw;}
else if(options.latitude&&options.longitude){options.center=new google.maps.LatLng(options.latitude,options.longitude);delete options.latitude;delete options.longitude;}
overlay.setOptions(options);},showHideOverlay:function(type,overlay,display){if(typeof display==='undefined'){if(this.getVisibleOverlay(type,overlay))
display=false;else
display=true;}
if(display)
$(this[this.overlays[type].id]).data(overlay).setMap(this.map);else
$(this[this.overlays[type].id]).data(overlay).setMap(null);},getVisibleOverlay:function(type,overlay){if($(this[this.overlays[type].id]).data(overlay).getMap())
return true;else
return false;},getOverlaysCount:function(type){return this[this.overlays[type].array].length;},removeOverlay:function(type,overlay){var index=$.inArray(overlay,this[this.overlays[type].array]),current;if(index>-1){current=this[this.overlays[type].array].splice(index,1);var markerId=current[0];$(this[this.overlays[type].id]).data(markerId).setMap(null);$(this[this.overlays[type].id]).removeData(markerId);return true;}
return false;},clearOverlays:function(type){for(var i=0,l=this[this.overlays[type].array].length;i<l;i++){var markerId=this[this.overlays[type].array][i];$(this[this.overlays[type].id]).data(markerId).setMap(null);$(this[this.overlays[type].id]).removeData(markerId);}
this[this.overlays[type].array]=[];},showHideMarker:function(marker,display){if(typeof display==='undefined'){if(this.getVisibleMarker(marker)){$(this.mapId).data(marker).setVisible(false);var info=$(this.mapId).data(marker+'info');if(info&&info.show){info.close();info.show=false;}}
else
$(this.mapId).data(marker).setVisible(true);}
else
$(this.mapId).data(marker).setVisible(display);},showHideMarkerByGroup:function(group,display){for(var i=0,l=this.markers.length;i<l;i++){var markerId=this.markers[i];var marker=$(this.mapId).data(markerId);if(marker.group==group){if(typeof display==='undefined'){if(this.getVisibleMarker(markerId)){marker.setVisible(false);var info=$(this.mapId).data(markerId+'info');if(info&&info.show){info.close();info.show=false;}}
else
marker.setVisible(true);}
else
marker.setVisible(display);}}},getVisibleMarker:function(marker){return $(this.mapId).data(marker).getVisible();},getMarkerCount:function(){return this.markers.length;},getTmpMarkerCount:function(){return this.tmpMarkers.length;},getVisibleMarkerCount:function(){return this.getMarkers('visiblesInMap').length;},getMarkerByGroupCount:function(group){return this.getMarkers('group',group).length;},getMarkers:function(type,name){var array=[];switch(type){case"json":for(var i=0,l=this.markers.length;i<l;i++){var temp="'"+i+"': '"+$(this.mapId).data(this.markers[i]).getPosition().toUrlValue()+"'";array.push(temp);}
array="{'markers':{"+array.join(",")+"}}";break;case"data":for(var i=0,l=this.markers.length;i<l;i++){var temp="marker["+i+"]="+$(this.mapId).data(this.markers[i]).getPosition().toUrlValue();array.push(temp);}
array=array.join("&");break;case"visiblesInBounds":for(var i=0,l=this.markers.length;i<l;i++){if(this.isVisible($(this.mapId).data(this.markers[i]).getPosition()))
array.push(this.markers[i]);}
break;case"visiblesInMap":for(var i=0,l=this.markers.length;i<l;i++){if(this.getVisibleMarker(this.markers[i]))
array.push(this.markers[i]);}
break;case"group":if(name)
for(var i=0,l=this.markers.length;i<l;i++){if($(this.mapId).data(this.markers[i]).group==name)
array.push(this.markers[i]);}
break;case"markers":for(var i=0,l=this.markers.length;i<l;i++){var temp=$(this.mapId).data(this.markers[i]);array.push(temp);}
break;default:for(var i=0,l=this.markers.length;i<l;i++){var temp=$(this.mapId).data(this.markers[i]).getPosition().toUrlValue();array.push(temp);}
break;}
return array;},getVisibleMarkers:function(){return this.getMarkers('visiblesInBounds');},createMarker:function(marker){if(!marker.geocode){this.count++;if(!marker.id)
marker.id=this.opts.prefixId+this.count;this.tmpMarkers.push(marker.id);}
if(marker.address&&!marker.geocode){this.geoMarkers.push(marker);this.geoMarker();}
else if(marker.latitude&&marker.longitude||marker.position){var options={map:this.map};options.id=marker.id;options.group=marker.group?marker.group:this.opts.groupId;options.zIndex=marker.zIndex?marker.zIndex:0;options.zIndexOrg=marker.zIndexOrg?marker.zIndexOrg:0;if(marker.visible==false)
options.visible=marker.visible;if(marker.title)
options.title=marker.title;if(marker.draggable)
options.draggable=marker.draggable;if(marker.icon&&marker.icon.image){options.icon=marker.icon.image;if(marker.icon.shadow)
options.shadow=marker.icon.shadow;}
else if(marker.icon)
options.icon=marker.icon;else if(this.opts.icon&&this.opts.icon.image){options.icon=this.opts.icon.image;if(this.opts.icon.shadow)
options.shadow=this.opts.icon.shadow;}
else if(this.opts.icon)
options.icon=this.opts.icon;options.position=marker.position?marker.position:new google.maps.LatLng(marker.latitude,marker.longitude);var cmarker=new google.maps.Marker(options);if(marker.html){if(!marker.html.content&&!marker.html.ajax&&!marker.html.id)
marker.html={content:marker.html};else if(!marker.html.content)
marker.html.content=null;this.setInfoWindow(cmarker,marker.html);}
this.addMarker(cmarker);return cmarker;}},addMarker:function(marker){$(this.mapId).data(marker.id,marker);this.markers.push(marker.id);},setMarker:function(marker,options){var tmarker=$(this.mapId).data(marker);delete options.id;delete options.visible;if(options.icon){var toption=options.icon;delete options.icon;if(toption&&toption=='default'){if(this.opts.icon&&this.opts.icon.image){options.icon=this.opts.icon.image;if(this.opts.icon.shadow)
options.shadow=this.opts.icon.shadow;}
else if(this.opts.icon)
options.icon=this.opts.icon;}
else if(toption&&toption.image){options.icon=toption.image;if(toption.shadow)
options.shadow=toption.shadow;}
else if(toption)
options.icon=toption;}
if(options.address){this.geocode({address:options.address},{markerId:tmarker});delete options.address;delete options.latitude;delete options.longitude;delete options.position;}
else if(options.latitude&&options.longitude||options.position){if(!options.position)
options.position=new google.maps.LatLng(options.latitude,options.longitude);}
tmarker.setOptions(options);},removeMarker:function(marker){var index=$.inArray(marker,this.markers),current;if(index>-1){this.tmpMarkers.splice(index,1);current=this.markers.splice(index,1);var markerId=current[0];var marker=$(this.mapId).data(markerId);var info=$(this.mapId).data(markerId+'info');marker.setVisible(false);marker.setMap(null);$(this.mapId).removeData(markerId);if(info){info.close();info.show=false;$(this.mapId).removeData(markerId+'info');}
return true;}
return false;},clearMarkers:function(){for(var i=0,l=this.markers.length;i<l;i++){var markerId=this.markers[i];var marker=$(this.mapId).data(markerId);var info=$(this.mapId).data(markerId+'info');marker.setVisible(false);marker.setMap(null);$(this.mapId).removeData(markerId);if(info){info.close();info.show=false;$(this.mapId).removeData(markerId+'info');}}
this.singleMarker=false;this.lockGeocode=false;this.markers=[];this.tmpMarkers=[];this.geoMarkers=[];},isVisible:function(latlng){return this.map.getBounds().contains(latlng);}}})(jQuery);;
/* Source and licensing information for the above line(s) can be found at https://palmettolasik.com/sites/all/modules/drupalexp/modules/dexp_gmap/js/jquery.gomap-1.3.2.js. */
/* Source and licensing information for the line(s) below can be found at https://palmettolasik.com/sites/all/modules/drupalexp/modules/dexp_gmap/js/dexp_gmap.js. */
jQuery(document).ready(function($){$container=$(".contact-map");var $gmaps=$(".gmap-area",$container);if($gmaps.length>0&&jQuery().goMap&&google&&google.maps){$gmaps.each(function(index,element){var addr=$gmaps.text(),z=$gmaps.data('zoom'),image=$gmaps.data('image'),title=$gmaps.data('title');var map=$(this).goMap({markers:[{address:addr,title:title,icon:{image:image}}],scrollwheel:false,zoom:z,maptype:'ROADMAP'});});var mapResize=false;$(window).resize(function(e){if(mapResize){clearTimeout(mapResize);}
mapResize=setTimeout(function(){if($.goMap.getMarkers('markers').length>0){$.goMap.map.panTo($.goMap.getMarkers('markers')[0].getPosition());};},100);});};});;
/* Source and licensing information for the above line(s) can be found at https://palmettolasik.com/sites/all/modules/drupalexp/modules/dexp_gmap/js/dexp_gmap.js. */
/* Source and licensing information for the line(s) below can be found at https://palmettolasik.com/sites/all/modules/jquery_ajax_load/jquery_ajax_load.js. */
(function($){Drupal.behaviors.jquery_ajax_load={attach:function(context,settings){jQuery.ajaxSetup({cache:false});var trigger=Drupal.settings.jquery_ajax_load.trigger;var target=Drupal.settings.jquery_ajax_load.target;$(trigger).once(function(){var html_string=$(this).attr('href');$(this).attr('href',target);var data_target=$(this).attr('data-target');if(typeof data_target==='undefined'){data_target=target;}
else{data_target='#'+data_target;}
$(this).click(function(evt){evt.preventDefault();jquery_ajax_load_load($(this),data_target,html_string);});});$(trigger).removeClass(trigger);}};function jquery_ajax_load_load(el,target,url){var module_path=Drupal.settings.jquery_ajax_load.module_path;var toggle=Drupal.settings.jquery_ajax_load.toggle;var base_path=Drupal.settings.jquery_ajax_load.base_path;var animation=Drupal.settings.jquery_ajax_load.animation;if(toggle&&$(el).hasClass("jquery_ajax_load_open")){$(el).removeClass("jquery_ajax_load_open");if(animation){$(target).hide('slow',function(){$(target).empty();});}
else{$(target).empty();}}
else{var loading_html=Drupal.t('Loading');loading_html+='... <img src="/';loading_html+=module_path;loading_html+='/jquery_ajax_load_loading.gif">';$(target).html(loading_html);$(target).load(base_path+'jquery_ajax_load/get'+url,function(response,status,xhr){if(status=="error"){var msg="Sorry but there was an error: ";$(target).html(msg+xhr.status+" "+xhr.statusText);}
else{if(animation){$(target).hide();$(target).show('slow')}}});$(el).addClass("jquery_ajax_load_open");}}}(jQuery));;
/* Source and licensing information for the above line(s) can be found at https://palmettolasik.com/sites/all/modules/jquery_ajax_load/jquery_ajax_load.js. */
/* Source and licensing information for the line(s) below can be found at https://palmettolasik.com/misc/progress.js. */
(function($){Drupal.progressBar=function(id,updateCallback,method,errorCallback){var pb=this;this.id=id;this.method=method||'GET';this.updateCallback=updateCallback;this.errorCallback=errorCallback;this.element=$('<div class="progress" aria-live="polite"></div>').attr('id',id);this.element.html('<div class="bar"><div class="filled"></div></div>'+'<div class="percentage"></div>'+'<div class="message">&nbsp;</div>');};Drupal.progressBar.prototype.setProgress=function(percentage,message){if(percentage>=0&&percentage<=100){$('div.filled',this.element).css('width',percentage+'%');$('div.percentage',this.element).html(percentage+'%');}
$('div.message',this.element).html(message);if(this.updateCallback){this.updateCallback(percentage,message,this);}};Drupal.progressBar.prototype.startMonitoring=function(uri,delay){this.delay=delay;this.uri=uri;this.sendPing();};Drupal.progressBar.prototype.stopMonitoring=function(){clearTimeout(this.timer);this.uri=null;};Drupal.progressBar.prototype.sendPing=function(){if(this.timer){clearTimeout(this.timer);}
if(this.uri){var pb=this;$.ajax({type:this.method,url:this.uri,data:'',dataType:'json',success:function(progress){if(progress.status==0){pb.displayError(progress.data);return;}
pb.setProgress(progress.percentage,progress.message);pb.timer=setTimeout(function(){pb.sendPing();},pb.delay);},error:function(xmlhttp){pb.displayError(Drupal.ajaxError(xmlhttp,pb.uri));}});}};Drupal.progressBar.prototype.displayError=function(string){var error=$('<div class="messages error"></div>').html(string);$(this.element).before(error).hide();if(this.errorCallback){this.errorCallback(this);}};})(jQuery);;
/* Source and licensing information for the above line(s) can be found at https://palmettolasik.com/misc/progress.js. */
/* Source and licensing information for the line(s) below can be found at https://palmettolasik.com/sites/all/modules/ctools/js/modal.js. */
(function($){Drupal.CTools=Drupal.CTools||{};Drupal.CTools.Modal=Drupal.CTools.Modal||{};Drupal.CTools.Modal.show=function(choice){var opts={};if(choice&&typeof choice=='string'&&Drupal.settings[choice]){$.extend(true,opts,Drupal.settings[choice]);}
else if(choice){$.extend(true,opts,choice);}
var defaults={modalTheme:'CToolsModalDialog',throbberTheme:'CToolsModalThrobber',animation:'show',animationSpeed:'fast',modalSize:{type:'scale',width:.8,height:.8,addWidth:0,addHeight:0,contentRight:25,contentBottom:45},modalOptions:{opacity:.55,background:'#fff'},modalClass:'default'};var settings={};$.extend(true,settings,defaults,Drupal.settings.CToolsModal,opts);if(Drupal.CTools.Modal.currentSettings&&Drupal.CTools.Modal.currentSettings!=settings){Drupal.CTools.Modal.modal.remove();Drupal.CTools.Modal.modal=null;}
Drupal.CTools.Modal.currentSettings=settings;var resize=function(e){var context=e?document:Drupal.CTools.Modal.modal;if(Drupal.CTools.Modal.currentSettings.modalSize.type=='scale'){var width=$(window).width()*Drupal.CTools.Modal.currentSettings.modalSize.width;var height=$(window).height()*Drupal.CTools.Modal.currentSettings.modalSize.height;}
else{var width=Drupal.CTools.Modal.currentSettings.modalSize.width;var height=Drupal.CTools.Modal.currentSettings.modalSize.height;}
$('div.ctools-modal-content',context).css({'width':width+Drupal.CTools.Modal.currentSettings.modalSize.addWidth+'px','height':height+Drupal.CTools.Modal.currentSettings.modalSize.addHeight+'px'});$('div.ctools-modal-content .modal-content',context).css({'width':(width-Drupal.CTools.Modal.currentSettings.modalSize.contentRight)+'px','height':(height-Drupal.CTools.Modal.currentSettings.modalSize.contentBottom)+'px'});}
if(!Drupal.CTools.Modal.modal){Drupal.CTools.Modal.modal=$(Drupal.theme(settings.modalTheme));if(settings.modalSize.type=='scale'){$(window).bind('resize',resize);}}
resize();$('span.modal-title',Drupal.CTools.Modal.modal).html(Drupal.CTools.Modal.currentSettings.loadingText);Drupal.CTools.Modal.modalContent(Drupal.CTools.Modal.modal,settings.modalOptions,settings.animation,settings.animationSpeed,settings.modalClass);$('#modalContent .modal-content').html(Drupal.theme(settings.throbberTheme)).addClass('ctools-modal-loading');$('#modalContent .modal-content').delegate('input.form-autocomplete','keyup',function(){$('#autocomplete').css('top',$(this).position().top+$(this).outerHeight()+$(this).offsetParent().filter('#modal-content').scrollTop());});};Drupal.CTools.Modal.dismiss=function(){if(Drupal.CTools.Modal.modal){Drupal.CTools.Modal.unmodalContent(Drupal.CTools.Modal.modal);}};Drupal.theme.prototype.CToolsModalDialog=function(){var html=''
html+='<div id="ctools-modal">'
html+='  <div class="ctools-modal-content">'
html+='    <div class="modal-header">';html+='      <a class="close" href="#">';html+=Drupal.CTools.Modal.currentSettings.closeText+Drupal.CTools.Modal.currentSettings.closeImage;html+='      </a>';html+='      <span id="modal-title" class="modal-title">&nbsp;</span>';html+='    </div>';html+='    <div id="modal-content" class="modal-content">';html+='    </div>';html+='  </div>';html+='</div>';return html;}
Drupal.theme.prototype.CToolsModalThrobber=function(){var html='';html+='<div id="modal-throbber">';html+='  <div class="modal-throbber-wrapper">';html+=Drupal.CTools.Modal.currentSettings.throbber;html+='  </div>';html+='</div>';return html;};Drupal.CTools.Modal.getSettings=function(object){var match=$(object).attr('class').match(/ctools-modal-(\S+)/);if(match){return match[1];}}
Drupal.CTools.Modal.clickAjaxCacheLink=function(){Drupal.CTools.Modal.show(Drupal.CTools.Modal.getSettings(this));return Drupal.CTools.AJAX.clickAJAXCacheLink.apply(this);};Drupal.CTools.Modal.clickAjaxLink=function(){Drupal.CTools.Modal.show(Drupal.CTools.Modal.getSettings(this));return false;};Drupal.CTools.Modal.submitAjaxForm=function(e){var $form=$(this);var url=$form.attr('action');setTimeout(function(){Drupal.CTools.AJAX.ajaxSubmit($form,url);},1);return false;}
Drupal.behaviors.ZZCToolsModal={attach:function(context){$('area.ctools-use-modal, a.ctools-use-modal',context).once('ctools-use-modal',function(){var $this=$(this);$this.click(Drupal.CTools.Modal.clickAjaxLink);var element_settings={};if($this.attr('href')){element_settings.url=$this.attr('href');element_settings.event='click';element_settings.progress={type:'throbber'};}
var base=$this.attr('href');Drupal.ajax[base]=new Drupal.ajax(base,this,element_settings);});$('input.ctools-use-modal, button.ctools-use-modal',context).once('ctools-use-modal',function(){var $this=$(this);$this.click(Drupal.CTools.Modal.clickAjaxLink);var button=this;var element_settings={};element_settings.url=Drupal.CTools.Modal.findURL(this);if(element_settings.url==''){element_settings.url=$(this).closest('form').attr('action');}
element_settings.event='click';element_settings.setClick=true;var base=$this.attr('id');Drupal.ajax[base]=new Drupal.ajax(base,this,element_settings);$('.'+$(button).attr('id')+'-url').change(function(){Drupal.ajax[base].options.url=Drupal.CTools.Modal.findURL(button);});});$('#modal-content form',context).once('ctools-use-modal',function(){var $this=$(this);var element_settings={};element_settings.url=$this.attr('action');element_settings.event='submit';element_settings.progress={'type':'throbber'}
var base=$this.attr('id');Drupal.ajax[base]=new Drupal.ajax(base,this,element_settings);Drupal.ajax[base].form=$this;$('input[type=submit], button',this).click(function(event){Drupal.ajax[base].element=this;this.form.clk=this;if(Drupal.autocompleteSubmit&&!Drupal.autocompleteSubmit()){return false;}
if(jQuery.fn.jquery==='1.4'&&typeof event.bubbles==="undefined"){$(this.form).trigger('submit');return false;}});});$('.ctools-close-modal',context).once('ctools-close-modal').click(function(){Drupal.CTools.Modal.dismiss();return false;});}};Drupal.CTools.Modal.modal_display=function(ajax,response,status){if($('#modalContent').length==0){Drupal.CTools.Modal.show(Drupal.CTools.Modal.getSettings(ajax.element));}
$('#modal-title').html(response.title);$('#modal-content').html(response.output).scrollTop(0);var settings=response.settings||ajax.settings||Drupal.settings;Drupal.attachBehaviors('#modalContent',settings);if($('#modal-content').hasClass('ctools-modal-loading')){$('#modal-content').removeClass('ctools-modal-loading');}
else{$('#modal-content :focusable:first').focus();}}
Drupal.CTools.Modal.modal_dismiss=function(command){Drupal.CTools.Modal.dismiss();$('link.ctools-temporary-css').remove();}
Drupal.CTools.Modal.modal_loading=function(command){Drupal.CTools.Modal.modal_display({output:Drupal.theme(Drupal.CTools.Modal.currentSettings.throbberTheme),title:Drupal.CTools.Modal.currentSettings.loadingText});}
Drupal.CTools.Modal.findURL=function(item){var url='';var url_class='.'+$(item).attr('id')+'-url';$(url_class).each(function(){var $this=$(this);if(url&&$this.val()){url+='/';}
url+=$this.val();});return url;};Drupal.CTools.Modal.modalContent=function(content,css,animation,speed,modalClass){if(!animation){animation='show';}
else{if(animation!='fadeIn'&&animation!='slideDown'){animation='show';}}
if(!speed){speed='fast';}
css=jQuery.extend({position:'absolute',left:'0px',margin:'0px',background:'#000',opacity:'.55'},css);css.filter='alpha(opacity='+(100*css.opacity)+')';content.hide();if($('#modalBackdrop').length)$('#modalBackdrop').remove();if($('#modalContent').length)$('#modalContent').remove();if(self.pageYOffset){var wt=self.pageYOffset;}else if(document.documentElement&&document.documentElement.scrollTop){var wt=document.documentElement.scrollTop;}else if(document.body){var wt=document.body.scrollTop;}
var docHeight=$(document).height()+50;var docWidth=$(document).width();var winHeight=$(window).height();var winWidth=$(window).width();if(docHeight<winHeight)docHeight=winHeight;$('body').append('<div id="modalBackdrop" class="backdrop-'+modalClass+'" style="z-index: 1000; display: none;"></div><div id="modalContent" class="modal-'+modalClass+'" style="z-index: 1001; position: absolute;">'+$(content).html()+'</div>');var getTabbableElements=function(){var tabbableElements=$('#modalContent :tabbable'),radioButtons=tabbableElements.filter('input[type="radio"]');if(radioButtons.length>0){var anySelected={};radioButtons.each(function(){var name=this.name;if(typeof anySelected[name]==='undefined'){anySelected[name]=radioButtons.filter('input[name="'+name+'"]:checked').length!==0;}});var found={};tabbableElements=tabbableElements.filter(function(){var keep=true;if(this.type=='radio'){if(anySelected[this.name]){keep=this.checked;}
else{if(found[this.name]){keep=false;}
found[this.name]=true;}}
return keep;});}
return tabbableElements.get();};modalEventHandler=function(event){target=null;if(event){target=event.target;}else{event=window.event;target=event.srcElement;}
var parents=$(target).parents().get();for(var i=0;i<parents.length;++i){var position=$(parents[i]).css('position');if(position=='absolute'||position=='fixed'){return true;}}
if($(target).is('#modalContent, body')||$(target).filter('*:visible').parents('#modalContent').length){return true;}
else{getTabbableElements()[0].focus();}
event.preventDefault();};$('body').bind('focus',modalEventHandler);$('body').bind('keypress',modalEventHandler);modalTabTrapHandler=function(evt){if(evt.which!=9){return true;}
var tabbableElements=getTabbableElements(),firstTabbableElement=tabbableElements[0],lastTabbableElement=tabbableElements[tabbableElements.length-1],singleTabbableElement=firstTabbableElement==lastTabbableElement,node=evt.target;if(node==firstTabbableElement&&evt.shiftKey){if(!singleTabbableElement){lastTabbableElement.focus();}
return false;}
else if(node==lastTabbableElement&&!evt.shiftKey){if(!singleTabbableElement){firstTabbableElement.focus();}
return false;}
else if($.inArray(node,tabbableElements)==-1){var parents=$(node).parents().get();for(var i=0;i<parents.length;++i){var position=$(parents[i]).css('position');if(position=='absolute'||position=='fixed'){return true;}}
if(evt.shiftKey){lastTabbableElement.focus();}
else{firstTabbableElement.focus();}}};$('body').bind('keydown',modalTabTrapHandler);var modalContent=$('#modalContent').css('top','-1000px');var mdcTop=wt+(winHeight / 2)-(modalContent.outerHeight()/ 2);var mdcLeft=(winWidth / 2)-(modalContent.outerWidth()/ 2);$('#modalBackdrop').css(css).css('top',0).css('height',docHeight+'px').css('width',docWidth+'px').show();modalContent.css({top:mdcTop+'px',left:mdcLeft+'px'}).hide()[animation](speed);modalContentClose=function(){close();return false;};$('.close').bind('click',modalContentClose);modalEventEscapeCloseHandler=function(event){if(event.keyCode==27){close();return false;}};$(document).bind('keydown',modalEventEscapeCloseHandler);var oldFocus=document.activeElement;$('.close').focus();function close(){$(window).unbind('resize',modalContentResize);$('body').unbind('focus',modalEventHandler);$('body').unbind('keypress',modalEventHandler);$('body').unbind('keydown',modalTabTrapHandler);$('.close').unbind('click',modalContentClose);$('body').unbind('keypress',modalEventEscapeCloseHandler);$(document).trigger('CToolsDetachBehaviors',$('#modalContent'));if(animation=='fadeIn')animation='fadeOut';if(animation=='slideDown')animation='slideUp';if(animation=='show')animation='hide';modalContent.hide()[animation](speed);$('#modalContent').remove();$('#modalBackdrop').remove();$(oldFocus).focus();};modalContentResize=function(){$('#modalBackdrop').css('height','').css('width','');if(self.pageYOffset){var wt=self.pageYOffset;}else if(document.documentElement&&document.documentElement.scrollTop){var wt=document.documentElement.scrollTop;}else if(document.body){var wt=document.body.scrollTop;}
var docHeight=$(document).height();var docWidth=$(document).width();var winHeight=$(window).height();var winWidth=$(window).width();if(docHeight<winHeight)docHeight=winHeight;var modalContent=$('#modalContent');var mdcTop=wt+(winHeight / 2)-(modalContent.outerHeight()/ 2);var mdcLeft=(winWidth / 2)-(modalContent.outerWidth()/ 2);$('#modalBackdrop').css('height',docHeight+'px').css('width',docWidth+'px').show();modalContent.css('top',mdcTop+'px').css('left',mdcLeft+'px').show();};$(window).bind('resize',modalContentResize);};Drupal.CTools.Modal.unmodalContent=function(content,animation,speed)
{if(!animation){var animation='show';}else{if((animation!='fadeOut')&&(animation!='slideUp'))animation='show';}
if(!speed)var speed='fast';$(window).unbind('resize',modalContentResize);$('body').unbind('focus',modalEventHandler);$('body').unbind('keypress',modalEventHandler);$('body').unbind('keydown',modalTabTrapHandler);$('.close').unbind('click',modalContentClose);$('body').unbind('keypress',modalEventEscapeCloseHandler);$(document).trigger('CToolsDetachBehaviors',$('#modalContent'));content.each(function(){if(animation=='fade'){$('#modalContent').fadeOut(speed,function(){$('#modalBackdrop').fadeOut(speed,function(){$(this).remove();});$(this).remove();});}else{if(animation=='slide'){$('#modalContent').slideUp(speed,function(){$('#modalBackdrop').slideUp(speed,function(){$(this).remove();});$(this).remove();});}else{$('#modalContent').remove();$('#modalBackdrop').remove();}}});};$(function(){Drupal.ajax.prototype.commands.modal_display=Drupal.CTools.Modal.modal_display;Drupal.ajax.prototype.commands.modal_dismiss=Drupal.CTools.Modal.modal_dismiss;});})(jQuery);;
/* Source and licensing information for the above line(s) can be found at https://palmettolasik.com/sites/all/modules/ctools/js/modal.js. */
/* Source and licensing information for the line(s) below can be found at https://palmettolasik.com/sites/all/modules/modal_forms/js/modal_forms_popup.js. */
Drupal.theme.prototype.ModalFormsPopup=function(){var html='';html+='<div id="ctools-modal" class="popups-box">';html+='  <div class="ctools-modal-content modal-forms-modal-content">';html+='    <div class="popups-container">';html+='      <div class="modal-header popups-title">';html+='        <span id="modal-title" class="modal-title"></span>';html+='        <span class="popups-close close">'+Drupal.CTools.Modal.currentSettings.closeText+'</span>';html+='        <div class="clear-block"></div>';html+='      </div>';html+='      <div class="modal-scroll"><div id="modal-content" class="modal-content popups-body"></div></div>';html+='    </div>';html+='  </div>';html+='</div>';return html;};
/* Source and licensing information for the above line(s) can be found at https://palmettolasik.com/sites/all/modules/modal_forms/js/modal_forms_popup.js. */
/* Source and licensing information for the line(s) below can be found at https://palmettolasik.com/sites/all/modules/twitter_bootstrap_modal/twitter_bootstrap_modal.js. */
(function($){Drupal.behaviors.twitter_bootstrap_modal={attach:function(context,settings){var TBtrigger=Drupal.settings.jquery_ajax_load.TBtrigger;$(TBtrigger).once(function(){var html_string=$(this).attr('href');$(this).attr('href','/jquery_ajax_load/get'+html_string);$(this).attr('data-target','#jquery_ajax_load');$(this).attr('data-toggle','modal');});var TBmodaltrigger=Drupal.settings.jquery_ajax_load.TBmodaltrigger;$(TBmodaltrigger).once(function(){$(this).click(openDialog);});}}
var openDialog=function(){var TBpath=Drupal.settings.jquery_ajax_load.TBpath;var html_string=TBpath+'bs_modal'+$(this).attr('href');$('#jquery_ajax_load').remove();twitter_bootstrap_modal_create_modal();$('#jquery_ajax_load').modal({remote:html_string});return false;}
function twitter_bootstrap_modal_create_modal(){var modal_name=Drupal.settings.jquery_ajax_load.TBname;var modal_module=Drupal.settings.jquery_ajax_load.TBmodule;$('body').append('<div id="jquery_ajax_load" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">');$('#jquery_ajax_load').append('<div class="modal-dialog" />');$('#jquery_ajax_load .modal-dialog').append('<div class="modal-content" />');$('#jquery_ajax_load .modal-content').append('<div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button><h3 class="modal_title">'+modal_name+'</h3></div>');$('#jquery_ajax_load .modal-content').append('<div class="modal-body"><span class="text-warning">'+Drupal.t('Loading')+'... </span><img src="/'+modal_module+'/twitter_bootstrap_modal_loading.gif"></div>');$('#jquery_ajax_load .modal-content').append('<div class="modal-footer"><button class="btn" data-dismiss="modal" aria-hidden="true">'+Drupal.t('Close')+'</button></div>');}}(jQuery));;
/* Source and licensing information for the above line(s) can be found at https://palmettolasik.com/sites/all/modules/twitter_bootstrap_modal/twitter_bootstrap_modal.js. */
/* Source and licensing information for the line(s) below can be found at https://palmettolasik.com/sites/all/modules/drupalexp/modules/dexp_menu/js/dexp-menu.js. */
jQuery(document).ready(function($){$.each($('.dexp-dropdown').find('a.w2, a.w3'),function(index,element){var w=$(element).attr('class').match(/w(\d)/);$(element).parent('li').find('>ul').addClass(w[0]);});$('.dexp-dropdown a.active').each(function(){$(this).parents('li.expanded').addClass('active');});$('.dexp-dropdown li.expanded').each(function(){var $this=$(this),$toggle=$('<span class="menu-toggler fa fa-angle-right"></span>');$toggle.click(function(){$(this).toggleClass('fa-angle-right fa-angle-down');$this.find('>ul').toggleClass('menu-open');});$this.append($toggle);});$('.dexp-menu-toggler').click(function(){var $menu=$($(this).data('target'));if($menu!=null){$menu.toggleClass('mobile-open');}
return false;});$('.dexp-dropdown ul ul li').hover(function(){var $submenu=$(this).find('>ul'),ww=$(window).width(),innerw=ww-(ww-$('.dexp-body-inner').width())/2;if($submenu.length==0)return;if($submenu.offset().left+$submenu.width()>innerw){$submenu.addClass('back');}},function(){var $submenu=$(this).find('>ul');if($submenu.length==0)return;$submenu.removeClass('back');});});;
/* Source and licensing information for the above line(s) can be found at https://palmettolasik.com/sites/all/modules/drupalexp/modules/dexp_menu/js/dexp-menu.js. */
/* Source and licensing information for the line(s) below can be found at https://palmettolasik.com/sites/all/libraries/jquery.cycle/jquery.cycle.all.js. */
;(function($,undefined){"use strict";var ver='2.9999.5';if($.support===undefined){$.support={opacity:!($.browser.msie)};}
function debug(s){if($.fn.cycle.debug)
log(s);}
function log(){if(window.console&&console.log)
console.log('[cycle] '+Array.prototype.join.call(arguments,' '));}
$.expr[':'].paused=function(el){return el.cyclePause;};$.fn.cycle=function(options,arg2){var o={s:this.selector,c:this.context};if(this.length===0&&options!='stop'){if(!$.isReady&&o.s){log('DOM not ready, queuing slideshow');$(function(){$(o.s,o.c).cycle(options,arg2);});return this;}
log('terminating; zero elements found by selector'+($.isReady?'':' (DOM not ready)'));return this;}
return this.each(function(){var opts=handleArguments(this,options,arg2);if(opts===false)
return;opts.updateActivePagerLink=opts.updateActivePagerLink||$.fn.cycle.updateActivePagerLink;if(this.cycleTimeout)
clearTimeout(this.cycleTimeout);this.cycleTimeout=this.cyclePause=0;this.cycleStop=0;var $cont=$(this);var $slides=opts.slideExpr?$(opts.slideExpr,this):$cont.children();var els=$slides.get();if(els.length<2){log('terminating; too few slides: '+els.length);return;}
var opts2=buildOptions($cont,$slides,els,opts,o);if(opts2===false)
return;var startTime=opts2.continuous?10:getTimeout(els[opts2.currSlide],els[opts2.nextSlide],opts2,!opts2.backwards);if(startTime){startTime+=(opts2.delay||0);if(startTime<10)
startTime=10;debug('first timeout: '+startTime);this.cycleTimeout=setTimeout(function(){go(els,opts2,0,!opts.backwards);},startTime);}});};function triggerPause(cont,byHover,onPager){var opts=$(cont).data('cycle.opts');var paused=!!cont.cyclePause;if(paused&&opts.paused)
opts.paused(cont,opts,byHover,onPager);else if(!paused&&opts.resumed)
opts.resumed(cont,opts,byHover,onPager);}
function handleArguments(cont,options,arg2){if(cont.cycleStop===undefined)
cont.cycleStop=0;if(options===undefined||options===null)
options={};if(options.constructor==String){switch(options){case'destroy':case'stop':var opts=$(cont).data('cycle.opts');if(!opts)
return false;cont.cycleStop++;if(cont.cycleTimeout)
clearTimeout(cont.cycleTimeout);cont.cycleTimeout=0;if(opts.elements)
$(opts.elements).stop();$(cont).removeData('cycle.opts');if(options=='destroy')
destroy(cont,opts);return false;case'toggle':cont.cyclePause=(cont.cyclePause===1)?0:1;checkInstantResume(cont.cyclePause,arg2,cont);triggerPause(cont);return false;case'pause':cont.cyclePause=1;triggerPause(cont);return false;case'resume':cont.cyclePause=0;checkInstantResume(false,arg2,cont);triggerPause(cont);return false;case'prev':case'next':opts=$(cont).data('cycle.opts');if(!opts){log('options not found, "prev/next" ignored');return false;}
$.fn.cycle[options](opts);return false;default:options={fx:options};}
return options;}
else if(options.constructor==Number){var num=options;options=$(cont).data('cycle.opts');if(!options){log('options not found, can not advance slide');return false;}
if(num<0||num>=options.elements.length){log('invalid slide index: '+num);return false;}
options.nextSlide=num;if(cont.cycleTimeout){clearTimeout(cont.cycleTimeout);cont.cycleTimeout=0;}
if(typeof arg2=='string')
options.oneTimeFx=arg2;go(options.elements,options,1,num>=options.currSlide);return false;}
return options;function checkInstantResume(isPaused,arg2,cont){if(!isPaused&&arg2===true){var options=$(cont).data('cycle.opts');if(!options){log('options not found, can not resume');return false;}
if(cont.cycleTimeout){clearTimeout(cont.cycleTimeout);cont.cycleTimeout=0;}
go(options.elements,options,1,!options.backwards);}}}
function removeFilter(el,opts){if(!$.support.opacity&&opts.cleartype&&el.style.filter){try{el.style.removeAttribute('filter');}
catch(smother){}}}
function destroy(cont,opts){if(opts.next)
$(opts.next).unbind(opts.prevNextEvent);if(opts.prev)
$(opts.prev).unbind(opts.prevNextEvent);if(opts.pager||opts.pagerAnchorBuilder)
$.each(opts.pagerAnchors||[],function(){this.unbind().remove();});opts.pagerAnchors=null;$(cont).unbind('mouseenter.cycle mouseleave.cycle');if(opts.destroy)
opts.destroy(opts);}
function buildOptions($cont,$slides,els,options,o){var startingSlideSpecified;var opts=$.extend({},$.fn.cycle.defaults,options||{},$.metadata?$cont.metadata():$.meta?$cont.data():{});var meta=$.isFunction($cont.data)?$cont.data(opts.metaAttr):null;if(meta)
opts=$.extend(opts,meta);if(opts.autostop)
opts.countdown=opts.autostopCount||els.length;var cont=$cont[0];$cont.data('cycle.opts',opts);opts.$cont=$cont;opts.stopCount=cont.cycleStop;opts.elements=els;opts.before=opts.before?[opts.before]:[];opts.after=opts.after?[opts.after]:[];if(!$.support.opacity&&opts.cleartype)
opts.after.push(function(){removeFilter(this,opts);});if(opts.continuous)
opts.after.push(function(){go(els,opts,0,!opts.backwards);});saveOriginalOpts(opts);if(!$.support.opacity&&opts.cleartype&&!opts.cleartypeNoBg)
clearTypeFix($slides);if($cont.css('position')=='static')
$cont.css('position','relative');if(opts.width)
$cont.width(opts.width);if(opts.height&&opts.height!='auto')
$cont.height(opts.height);if(opts.startingSlide!==undefined){opts.startingSlide=parseInt(opts.startingSlide,10);if(opts.startingSlide>=els.length||opts.startSlide<0)
opts.startingSlide=0;else
startingSlideSpecified=true;}
else if(opts.backwards)
opts.startingSlide=els.length-1;else
opts.startingSlide=0;if(opts.random){opts.randomMap=[];for(var i=0;i<els.length;i++)
opts.randomMap.push(i);opts.randomMap.sort(function(a,b){return Math.random()-0.5;});if(startingSlideSpecified){for(var cnt=0;cnt<els.length;cnt++){if(opts.startingSlide==opts.randomMap[cnt]){opts.randomIndex=cnt;}}}
else{opts.randomIndex=1;opts.startingSlide=opts.randomMap[1];}}
else if(opts.startingSlide>=els.length)
opts.startingSlide=0;opts.currSlide=opts.startingSlide||0;var first=opts.startingSlide;$slides.css({position:'absolute',top:0,left:0}).hide().each(function(i){var z;if(opts.backwards)
z=first?i<=first?els.length+(i-first):first-i:els.length-i;else
z=first?i>=first?els.length-(i-first):first-i:els.length-i;$(this).css('z-index',z);});$(els[first]).css('opacity',1).show();removeFilter(els[first],opts);if(opts.fit){if(!opts.aspect){if(opts.width)
$slides.width(opts.width);if(opts.height&&opts.height!='auto')
$slides.height(opts.height);}else{$slides.each(function(){var $slide=$(this);var ratio=(opts.aspect===true)?$slide.width()/$slide.height():opts.aspect;if(opts.width&&$slide.width()!=opts.width){$slide.width(opts.width);$slide.height(opts.width / ratio);}
if(opts.height&&$slide.height()<opts.height){$slide.height(opts.height);$slide.width(opts.height*ratio);}});}}
if(opts.center&&((!opts.fit)||opts.aspect)){$slides.each(function(){var $slide=$(this);$slide.css({"margin-left":opts.width?((opts.width-$slide.width())/ 2)+"px":0,"margin-top":opts.height?((opts.height-$slide.height())/ 2)+"px":0});});}
if(opts.center&&!opts.fit&&!opts.slideResize){$slides.each(function(){var $slide=$(this);$slide.css({"margin-left":opts.width?((opts.width-$slide.width())/ 2)+"px":0,"margin-top":opts.height?((opts.height-$slide.height())/ 2)+"px":0});});}
var reshape=opts.containerResize&&!$cont.innerHeight();if(reshape){var maxw=0,maxh=0;for(var j=0;j<els.length;j++){var $e=$(els[j]),e=$e[0],w=$e.outerWidth(),h=$e.outerHeight();if(!w)w=e.offsetWidth||e.width||$e.attr('width');if(!h)h=e.offsetHeight||e.height||$e.attr('height');maxw=w>maxw?w:maxw;maxh=h>maxh?h:maxh;}
if(maxw>0&&maxh>0)
$cont.css({width:maxw+'px',height:maxh+'px'});}
var pauseFlag=false;if(opts.pause)
$cont.bind('mouseenter.cycle',function(){pauseFlag=true;this.cyclePause++;triggerPause(cont,true);}).bind('mouseleave.cycle',function(){if(pauseFlag)
this.cyclePause--;triggerPause(cont,true);});if(supportMultiTransitions(opts)===false)
return false;var requeue=false;options.requeueAttempts=options.requeueAttempts||0;$slides.each(function(){var $el=$(this);this.cycleH=(opts.fit&&opts.height)?opts.height:($el.height()||this.offsetHeight||this.height||$el.attr('height')||0);this.cycleW=(opts.fit&&opts.width)?opts.width:($el.width()||this.offsetWidth||this.width||$el.attr('width')||0);if($el.is('img')){var loadingIE=($.browser.msie&&this.cycleW==28&&this.cycleH==30&&!this.complete);var loadingFF=($.browser.mozilla&&this.cycleW==34&&this.cycleH==19&&!this.complete);var loadingOp=($.browser.opera&&((this.cycleW==42&&this.cycleH==19)||(this.cycleW==37&&this.cycleH==17))&&!this.complete);var loadingOther=(this.cycleH===0&&this.cycleW===0&&!this.complete);if(loadingIE||loadingFF||loadingOp||loadingOther){if(o.s&&opts.requeueOnImageNotLoaded&&++options.requeueAttempts<100){log(options.requeueAttempts,' - img slide not loaded, requeuing slideshow: ',this.src,this.cycleW,this.cycleH);setTimeout(function(){$(o.s,o.c).cycle(options);},opts.requeueTimeout);requeue=true;return false;}
else{log('could not determine size of image: '+this.src,this.cycleW,this.cycleH);}}}
return true;});if(requeue)
return false;opts.cssBefore=opts.cssBefore||{};opts.cssAfter=opts.cssAfter||{};opts.cssFirst=opts.cssFirst||{};opts.animIn=opts.animIn||{};opts.animOut=opts.animOut||{};$slides.not(':eq('+first+')').css(opts.cssBefore);$($slides[first]).css(opts.cssFirst);if(opts.timeout){opts.timeout=parseInt(opts.timeout,10);if(opts.speed.constructor==String)
opts.speed=$.fx.speeds[opts.speed]||parseInt(opts.speed,10);if(!opts.sync)
opts.speed=opts.speed / 2;var buffer=opts.fx=='none'?0:opts.fx=='shuffle'?500:250;while((opts.timeout-opts.speed)<buffer)
opts.timeout+=opts.speed;}
if(opts.easing)
opts.easeIn=opts.easeOut=opts.easing;if(!opts.speedIn)
opts.speedIn=opts.speed;if(!opts.speedOut)
opts.speedOut=opts.speed;opts.slideCount=els.length;opts.currSlide=opts.lastSlide=first;if(opts.random){if(++opts.randomIndex==els.length)
opts.randomIndex=0;opts.nextSlide=opts.randomMap[opts.randomIndex];}
else if(opts.backwards)
opts.nextSlide=opts.startingSlide===0?(els.length-1):opts.startingSlide-1;else
opts.nextSlide=opts.startingSlide>=(els.length-1)?0:opts.startingSlide+1;if(!opts.multiFx){var init=$.fn.cycle.transitions[opts.fx];if($.isFunction(init))
init($cont,$slides,opts);else if(opts.fx!='custom'&&!opts.multiFx){log('unknown transition: '+opts.fx,'; slideshow terminating');return false;}}
var e0=$slides[first];if(!opts.skipInitializationCallbacks){if(opts.before.length)
opts.before[0].apply(e0,[e0,e0,opts,true]);if(opts.after.length)
opts.after[0].apply(e0,[e0,e0,opts,true]);}
if(opts.next)
$(opts.next).bind(opts.prevNextEvent,function(){return advance(opts,1);});if(opts.prev)
$(opts.prev).bind(opts.prevNextEvent,function(){return advance(opts,0);});if(opts.pager||opts.pagerAnchorBuilder)
buildPager(els,opts);exposeAddSlide(opts,els);return opts;}
function saveOriginalOpts(opts){opts.original={before:[],after:[]};opts.original.cssBefore=$.extend({},opts.cssBefore);opts.original.cssAfter=$.extend({},opts.cssAfter);opts.original.animIn=$.extend({},opts.animIn);opts.original.animOut=$.extend({},opts.animOut);$.each(opts.before,function(){opts.original.before.push(this);});$.each(opts.after,function(){opts.original.after.push(this);});}
function supportMultiTransitions(opts){var i,tx,txs=$.fn.cycle.transitions;if(opts.fx.indexOf(',')>0){opts.multiFx=true;opts.fxs=opts.fx.replace(/\s*/g,'').split(',');for(i=0;i<opts.fxs.length;i++){var fx=opts.fxs[i];tx=txs[fx];if(!tx||!txs.hasOwnProperty(fx)||!$.isFunction(tx)){log('discarding unknown transition: ',fx);opts.fxs.splice(i,1);i--;}}
if(!opts.fxs.length){log('No valid transitions named; slideshow terminating.');return false;}}
else if(opts.fx=='all'){opts.multiFx=true;opts.fxs=[];for(var p in txs){if(txs.hasOwnProperty(p)){tx=txs[p];if(txs.hasOwnProperty(p)&&$.isFunction(tx))
opts.fxs.push(p);}}}
if(opts.multiFx&&opts.randomizeEffects){var r1=Math.floor(Math.random()*20)+30;for(i=0;i<r1;i++){var r2=Math.floor(Math.random()*opts.fxs.length);opts.fxs.push(opts.fxs.splice(r2,1)[0]);}
debug('randomized fx sequence: ',opts.fxs);}
return true;}
function exposeAddSlide(opts,els){opts.addSlide=function(newSlide,prepend){var $s=$(newSlide),s=$s[0];if(!opts.autostopCount)
opts.countdown++;els[prepend?'unshift':'push'](s);if(opts.els)
opts.els[prepend?'unshift':'push'](s);opts.slideCount=els.length;if(opts.random){opts.randomMap.push(opts.slideCount-1);opts.randomMap.sort(function(a,b){return Math.random()-0.5;});}
$s.css('position','absolute');$s[prepend?'prependTo':'appendTo'](opts.$cont);if(prepend){opts.currSlide++;opts.nextSlide++;}
if(!$.support.opacity&&opts.cleartype&&!opts.cleartypeNoBg)
clearTypeFix($s);if(opts.fit&&opts.width)
$s.width(opts.width);if(opts.fit&&opts.height&&opts.height!='auto')
$s.height(opts.height);s.cycleH=(opts.fit&&opts.height)?opts.height:$s.height();s.cycleW=(opts.fit&&opts.width)?opts.width:$s.width();$s.css(opts.cssBefore);if(opts.pager||opts.pagerAnchorBuilder)
$.fn.cycle.createPagerAnchor(els.length-1,s,$(opts.pager),els,opts);if($.isFunction(opts.onAddSlide))
opts.onAddSlide($s);else
$s.hide();};}
$.fn.cycle.resetState=function(opts,fx){fx=fx||opts.fx;opts.before=[];opts.after=[];opts.cssBefore=$.extend({},opts.original.cssBefore);opts.cssAfter=$.extend({},opts.original.cssAfter);opts.animIn=$.extend({},opts.original.animIn);opts.animOut=$.extend({},opts.original.animOut);opts.fxFn=null;$.each(opts.original.before,function(){opts.before.push(this);});$.each(opts.original.after,function(){opts.after.push(this);});var init=$.fn.cycle.transitions[fx];if($.isFunction(init))
init(opts.$cont,$(opts.elements),opts);};function go(els,opts,manual,fwd){var p=opts.$cont[0],curr=els[opts.currSlide],next=els[opts.nextSlide];if(manual&&opts.busy&&opts.manualTrump){debug('manualTrump in go(), stopping active transition');$(els).stop(true,true);opts.busy=0;clearTimeout(p.cycleTimeout);}
if(opts.busy){debug('transition active, ignoring new tx request');return;}
if(p.cycleStop!=opts.stopCount||p.cycleTimeout===0&&!manual)
return;if(!manual&&!p.cyclePause&&!opts.bounce&&((opts.autostop&&(--opts.countdown<=0))||(opts.nowrap&&!opts.random&&opts.nextSlide<opts.currSlide))){if(opts.end)
opts.end(opts);return;}
var changed=false;if((manual||!p.cyclePause)&&(opts.nextSlide!=opts.currSlide)){changed=true;var fx=opts.fx;curr.cycleH=curr.cycleH||$(curr).height();curr.cycleW=curr.cycleW||$(curr).width();next.cycleH=next.cycleH||$(next).height();next.cycleW=next.cycleW||$(next).width();if(opts.multiFx){if(fwd&&(opts.lastFx===undefined||++opts.lastFx>=opts.fxs.length))
opts.lastFx=0;else if(!fwd&&(opts.lastFx===undefined||--opts.lastFx<0))
opts.lastFx=opts.fxs.length-1;fx=opts.fxs[opts.lastFx];}
if(opts.oneTimeFx){fx=opts.oneTimeFx;opts.oneTimeFx=null;}
$.fn.cycle.resetState(opts,fx);if(opts.before.length)
$.each(opts.before,function(i,o){if(p.cycleStop!=opts.stopCount)return;o.apply(next,[curr,next,opts,fwd]);});var after=function(){opts.busy=0;$.each(opts.after,function(i,o){if(p.cycleStop!=opts.stopCount)return;o.apply(next,[curr,next,opts,fwd]);});if(!p.cycleStop){queueNext();}};debug('tx firing('+fx+'); currSlide: '+opts.currSlide+'; nextSlide: '+opts.nextSlide);opts.busy=1;if(opts.fxFn)
opts.fxFn(curr,next,opts,after,fwd,manual&&opts.fastOnEvent);else if($.isFunction($.fn.cycle[opts.fx]))
$.fn.cycle[opts.fx](curr,next,opts,after,fwd,manual&&opts.fastOnEvent);else
$.fn.cycle.custom(curr,next,opts,after,fwd,manual&&opts.fastOnEvent);}
else{queueNext();}
if(changed||opts.nextSlide==opts.currSlide){var roll;opts.lastSlide=opts.currSlide;if(opts.random){opts.currSlide=opts.nextSlide;if(++opts.randomIndex==els.length){opts.randomIndex=0;opts.randomMap.sort(function(a,b){return Math.random()-0.5;});}
opts.nextSlide=opts.randomMap[opts.randomIndex];if(opts.nextSlide==opts.currSlide)
opts.nextSlide=(opts.currSlide==opts.slideCount-1)?0:opts.currSlide+1;}
else if(opts.backwards){roll=(opts.nextSlide-1)<0;if(roll&&opts.bounce){opts.backwards=!opts.backwards;opts.nextSlide=1;opts.currSlide=0;}
else{opts.nextSlide=roll?(els.length-1):opts.nextSlide-1;opts.currSlide=roll?0:opts.nextSlide+1;}}
else{roll=(opts.nextSlide+1)==els.length;if(roll&&opts.bounce){opts.backwards=!opts.backwards;opts.nextSlide=els.length-2;opts.currSlide=els.length-1;}
else{opts.nextSlide=roll?0:opts.nextSlide+1;opts.currSlide=roll?els.length-1:opts.nextSlide-1;}}}
if(changed&&opts.pager)
opts.updateActivePagerLink(opts.pager,opts.currSlide,opts.activePagerClass);function queueNext(){var ms=0,timeout=opts.timeout;if(opts.timeout&&!opts.continuous){ms=getTimeout(els[opts.currSlide],els[opts.nextSlide],opts,fwd);if(opts.fx=='shuffle')
ms-=opts.speedOut;}
else if(opts.continuous&&p.cyclePause)
ms=10;if(ms>0)
p.cycleTimeout=setTimeout(function(){go(els,opts,0,!opts.backwards);},ms);}}
$.fn.cycle.updateActivePagerLink=function(pager,currSlide,clsName){$(pager).each(function(){$(this).children().removeClass(clsName).eq(currSlide).addClass(clsName);});};function getTimeout(curr,next,opts,fwd){if(opts.timeoutFn){var t=opts.timeoutFn.call(curr,curr,next,opts,fwd);while(opts.fx!='none'&&(t-opts.speed)<250)
t+=opts.speed;debug('calculated timeout: '+t+'; speed: '+opts.speed);if(t!==false)
return t;}
return opts.timeout;}
$.fn.cycle.next=function(opts){advance(opts,1);};$.fn.cycle.prev=function(opts){advance(opts,0);};function advance(opts,moveForward){var val=moveForward?1:-1;var els=opts.elements;var p=opts.$cont[0],timeout=p.cycleTimeout;if(timeout){clearTimeout(timeout);p.cycleTimeout=0;}
if(opts.random&&val<0){opts.randomIndex--;if(--opts.randomIndex==-2)
opts.randomIndex=els.length-2;else if(opts.randomIndex==-1)
opts.randomIndex=els.length-1;opts.nextSlide=opts.randomMap[opts.randomIndex];}
else if(opts.random){opts.nextSlide=opts.randomMap[opts.randomIndex];}
else{opts.nextSlide=opts.currSlide+val;if(opts.nextSlide<0){if(opts.nowrap)return false;opts.nextSlide=els.length-1;}
else if(opts.nextSlide>=els.length){if(opts.nowrap)return false;opts.nextSlide=0;}}
var cb=opts.onPrevNextEvent||opts.prevNextClick;if($.isFunction(cb))
cb(val>0,opts.nextSlide,els[opts.nextSlide]);go(els,opts,1,moveForward);return false;}
function buildPager(els,opts){var $p=$(opts.pager);$.each(els,function(i,o){$.fn.cycle.createPagerAnchor(i,o,$p,els,opts);});opts.updateActivePagerLink(opts.pager,opts.startingSlide,opts.activePagerClass);}
$.fn.cycle.createPagerAnchor=function(i,el,$p,els,opts){var a;if($.isFunction(opts.pagerAnchorBuilder)){a=opts.pagerAnchorBuilder(i,el);debug('pagerAnchorBuilder('+i+', el) returned: '+a);}
else
a='<a href="#">'+(i+1)+'</a>';if(!a)
return;var $a=$(a);if($a.parents('body').length===0){var arr=[];if($p.length>1){$p.each(function(){var $clone=$a.clone(true);$(this).append($clone);arr.push($clone[0]);});$a=$(arr);}
else{$a.appendTo($p);}}
opts.pagerAnchors=opts.pagerAnchors||[];opts.pagerAnchors.push($a);var pagerFn=function(e){e.preventDefault();opts.nextSlide=i;var p=opts.$cont[0],timeout=p.cycleTimeout;if(timeout){clearTimeout(timeout);p.cycleTimeout=0;}
var cb=opts.onPagerEvent||opts.pagerClick;if($.isFunction(cb))
cb(opts.nextSlide,els[opts.nextSlide]);go(els,opts,1,opts.currSlide<i);};if(/mouseenter|mouseover/i.test(opts.pagerEvent)){$a.hover(pagerFn,function(){});}
else{$a.bind(opts.pagerEvent,pagerFn);}
if(!/^click/.test(opts.pagerEvent)&&!opts.allowPagerClickBubble)
$a.bind('click.cycle',function(){return false;});var cont=opts.$cont[0];var pauseFlag=false;if(opts.pauseOnPagerHover){$a.hover(function(){pauseFlag=true;cont.cyclePause++;triggerPause(cont,true,true);},function(){if(pauseFlag)
cont.cyclePause--;triggerPause(cont,true,true);});}};$.fn.cycle.hopsFromLast=function(opts,fwd){var hops,l=opts.lastSlide,c=opts.currSlide;if(fwd)
hops=c>l?c-l:opts.slideCount-l;else
hops=c<l?l-c:l+opts.slideCount-c;return hops;};function clearTypeFix($slides){debug('applying clearType background-color hack');function hex(s){s=parseInt(s,10).toString(16);return s.length<2?'0'+s:s;}
function getBg(e){for(;e&&e.nodeName.toLowerCase()!='html';e=e.parentNode){var v=$.css(e,'background-color');if(v&&v.indexOf('rgb')>=0){var rgb=v.match(/\d+/g);return'#'+hex(rgb[0])+hex(rgb[1])+hex(rgb[2]);}
if(v&&v!='transparent')
return v;}
return'#ffffff';}
$slides.each(function(){$(this).css('background-color',getBg(this));});}
$.fn.cycle.commonReset=function(curr,next,opts,w,h,rev){$(opts.elements).not(curr).hide();if(typeof opts.cssBefore.opacity=='undefined')
opts.cssBefore.opacity=1;opts.cssBefore.display='block';if(opts.slideResize&&w!==false&&next.cycleW>0)
opts.cssBefore.width=next.cycleW;if(opts.slideResize&&h!==false&&next.cycleH>0)
opts.cssBefore.height=next.cycleH;opts.cssAfter=opts.cssAfter||{};opts.cssAfter.display='none';$(curr).css('zIndex',opts.slideCount+(rev===true?1:0));$(next).css('zIndex',opts.slideCount+(rev===true?0:1));};$.fn.cycle.custom=function(curr,next,opts,cb,fwd,speedOverride){var $l=$(curr),$n=$(next);var speedIn=opts.speedIn,speedOut=opts.speedOut,easeIn=opts.easeIn,easeOut=opts.easeOut;$n.css(opts.cssBefore);if(speedOverride){if(typeof speedOverride=='number')
speedIn=speedOut=speedOverride;else
speedIn=speedOut=1;easeIn=easeOut=null;}
var fn=function(){$n.animate(opts.animIn,speedIn,easeIn,function(){cb();});};$l.animate(opts.animOut,speedOut,easeOut,function(){$l.css(opts.cssAfter);if(!opts.sync)
fn();});if(opts.sync)fn();};$.fn.cycle.transitions={fade:function($cont,$slides,opts){$slides.not(':eq('+opts.currSlide+')').css('opacity',0);opts.before.push(function(curr,next,opts){$.fn.cycle.commonReset(curr,next,opts);opts.cssBefore.opacity=0;});opts.animIn={opacity:1};opts.animOut={opacity:0};opts.cssBefore={top:0,left:0};}};$.fn.cycle.ver=function(){return ver;};$.fn.cycle.defaults={activePagerClass:'activeSlide',after:null,allowPagerClickBubble:false,animIn:null,animOut:null,aspect:false,autostop:0,autostopCount:0,backwards:false,before:null,center:null,cleartype:!$.support.opacity,cleartypeNoBg:false,containerResize:1,continuous:0,cssAfter:null,cssBefore:null,delay:0,easeIn:null,easeOut:null,easing:null,end:null,fastOnEvent:0,fit:0,fx:'fade',fxFn:null,height:'auto',manualTrump:true,metaAttr:'cycle',next:null,nowrap:0,onPagerEvent:null,onPrevNextEvent:null,pager:null,pagerAnchorBuilder:null,pagerEvent:'click.cycle',pause:0,pauseOnPagerHover:0,prev:null,prevNextEvent:'click.cycle',random:0,randomizeEffects:1,requeueOnImageNotLoaded:true,requeueTimeout:250,rev:0,shuffle:null,skipInitializationCallbacks:false,slideExpr:null,slideResize:1,speed:1000,speedIn:null,speedOut:null,startingSlide:undefined,sync:1,timeout:4000,timeoutFn:null,updateActivePagerLink:null,width:null};})(jQuery);(function($){"use strict";$.fn.cycle.transitions.none=function($cont,$slides,opts){opts.fxFn=function(curr,next,opts,after){$(next).show();$(curr).hide();after();};};$.fn.cycle.transitions.fadeout=function($cont,$slides,opts){$slides.not(':eq('+opts.currSlide+')').css({display:'block','opacity':1});opts.before.push(function(curr,next,opts,w,h,rev){$(curr).css('zIndex',opts.slideCount+(rev!==true?1:0));$(next).css('zIndex',opts.slideCount+(rev!==true?0:1));});opts.animIn.opacity=1;opts.animOut.opacity=0;opts.cssBefore.opacity=1;opts.cssBefore.display='block';opts.cssAfter.zIndex=0;};$.fn.cycle.transitions.scrollUp=function($cont,$slides,opts){$cont.css('overflow','hidden');opts.before.push($.fn.cycle.commonReset);var h=$cont.height();opts.cssBefore.top=h;opts.cssBefore.left=0;opts.cssFirst.top=0;opts.animIn.top=0;opts.animOut.top=-h;};$.fn.cycle.transitions.scrollDown=function($cont,$slides,opts){$cont.css('overflow','hidden');opts.before.push($.fn.cycle.commonReset);var h=$cont.height();opts.cssFirst.top=0;opts.cssBefore.top=-h;opts.cssBefore.left=0;opts.animIn.top=0;opts.animOut.top=h;};$.fn.cycle.transitions.scrollLeft=function($cont,$slides,opts){$cont.css('overflow','hidden');opts.before.push($.fn.cycle.commonReset);var w=$cont.width();opts.cssFirst.left=0;opts.cssBefore.left=w;opts.cssBefore.top=0;opts.animIn.left=0;opts.animOut.left=0-w;};$.fn.cycle.transitions.scrollRight=function($cont,$slides,opts){$cont.css('overflow','hidden');opts.before.push($.fn.cycle.commonReset);var w=$cont.width();opts.cssFirst.left=0;opts.cssBefore.left=-w;opts.cssBefore.top=0;opts.animIn.left=0;opts.animOut.left=w;};$.fn.cycle.transitions.scrollHorz=function($cont,$slides,opts){$cont.css('overflow','hidden').width();opts.before.push(function(curr,next,opts,fwd){if(opts.rev)
fwd=!fwd;$.fn.cycle.commonReset(curr,next,opts);opts.cssBefore.left=fwd?(next.cycleW-1):(1-next.cycleW);opts.animOut.left=fwd?-curr.cycleW:curr.cycleW;});opts.cssFirst.left=0;opts.cssBefore.top=0;opts.animIn.left=0;opts.animOut.top=0;};$.fn.cycle.transitions.scrollVert=function($cont,$slides,opts){$cont.css('overflow','hidden');opts.before.push(function(curr,next,opts,fwd){if(opts.rev)
fwd=!fwd;$.fn.cycle.commonReset(curr,next,opts);opts.cssBefore.top=fwd?(1-next.cycleH):(next.cycleH-1);opts.animOut.top=fwd?curr.cycleH:-curr.cycleH;});opts.cssFirst.top=0;opts.cssBefore.left=0;opts.animIn.top=0;opts.animOut.left=0;};$.fn.cycle.transitions.slideX=function($cont,$slides,opts){opts.before.push(function(curr,next,opts){$(opts.elements).not(curr).hide();$.fn.cycle.commonReset(curr,next,opts,false,true);opts.animIn.width=next.cycleW;});opts.cssBefore.left=0;opts.cssBefore.top=0;opts.cssBefore.width=0;opts.animIn.width='show';opts.animOut.width=0;};$.fn.cycle.transitions.slideY=function($cont,$slides,opts){opts.before.push(function(curr,next,opts){$(opts.elements).not(curr).hide();$.fn.cycle.commonReset(curr,next,opts,true,false);opts.animIn.height=next.cycleH;});opts.cssBefore.left=0;opts.cssBefore.top=0;opts.cssBefore.height=0;opts.animIn.height='show';opts.animOut.height=0;};$.fn.cycle.transitions.shuffle=function($cont,$slides,opts){var i,w=$cont.css('overflow','visible').width();$slides.css({left:0,top:0});opts.before.push(function(curr,next,opts){$.fn.cycle.commonReset(curr,next,opts,true,true,true);});if(!opts.speedAdjusted){opts.speed=opts.speed / 2;opts.speedAdjusted=true;}
opts.random=0;opts.shuffle=opts.shuffle||{left:-w,top:15};opts.els=[];for(i=0;i<$slides.length;i++)
opts.els.push($slides[i]);for(i=0;i<opts.currSlide;i++)
opts.els.push(opts.els.shift());opts.fxFn=function(curr,next,opts,cb,fwd){if(opts.rev)
fwd=!fwd;var $el=fwd?$(curr):$(next);$(next).css(opts.cssBefore);var count=opts.slideCount;$el.animate(opts.shuffle,opts.speedIn,opts.easeIn,function(){var hops=$.fn.cycle.hopsFromLast(opts,fwd);for(var k=0;k<hops;k++){if(fwd)
opts.els.push(opts.els.shift());else
opts.els.unshift(opts.els.pop());}
if(fwd){for(var i=0,len=opts.els.length;i<len;i++)
$(opts.els[i]).css('z-index',len-i+count);}
else{var z=$(curr).css('z-index');$el.css('z-index',parseInt(z,10)+1+count);}
$el.animate({left:0,top:0},opts.speedOut,opts.easeOut,function(){$(fwd?this:curr).hide();if(cb)cb();});});};$.extend(opts.cssBefore,{display:'block',opacity:1,top:0,left:0});};$.fn.cycle.transitions.turnUp=function($cont,$slides,opts){opts.before.push(function(curr,next,opts){$.fn.cycle.commonReset(curr,next,opts,true,false);opts.cssBefore.top=next.cycleH;opts.animIn.height=next.cycleH;opts.animOut.width=next.cycleW;});opts.cssFirst.top=0;opts.cssBefore.left=0;opts.cssBefore.height=0;opts.animIn.top=0;opts.animOut.height=0;};$.fn.cycle.transitions.turnDown=function($cont,$slides,opts){opts.before.push(function(curr,next,opts){$.fn.cycle.commonReset(curr,next,opts,true,false);opts.animIn.height=next.cycleH;opts.animOut.top=curr.cycleH;});opts.cssFirst.top=0;opts.cssBefore.left=0;opts.cssBefore.top=0;opts.cssBefore.height=0;opts.animOut.height=0;};$.fn.cycle.transitions.turnLeft=function($cont,$slides,opts){opts.before.push(function(curr,next,opts){$.fn.cycle.commonReset(curr,next,opts,false,true);opts.cssBefore.left=next.cycleW;opts.animIn.width=next.cycleW;});opts.cssBefore.top=0;opts.cssBefore.width=0;opts.animIn.left=0;opts.animOut.width=0;};$.fn.cycle.transitions.turnRight=function($cont,$slides,opts){opts.before.push(function(curr,next,opts){$.fn.cycle.commonReset(curr,next,opts,false,true);opts.animIn.width=next.cycleW;opts.animOut.left=curr.cycleW;});$.extend(opts.cssBefore,{top:0,left:0,width:0});opts.animIn.left=0;opts.animOut.width=0;};$.fn.cycle.transitions.zoom=function($cont,$slides,opts){opts.before.push(function(curr,next,opts){$.fn.cycle.commonReset(curr,next,opts,false,false,true);opts.cssBefore.top=next.cycleH/2;opts.cssBefore.left=next.cycleW/2;$.extend(opts.animIn,{top:0,left:0,width:next.cycleW,height:next.cycleH});$.extend(opts.animOut,{width:0,height:0,top:curr.cycleH/2,left:curr.cycleW/2});});opts.cssFirst.top=0;opts.cssFirst.left=0;opts.cssBefore.width=0;opts.cssBefore.height=0;};$.fn.cycle.transitions.fadeZoom=function($cont,$slides,opts){opts.before.push(function(curr,next,opts){$.fn.cycle.commonReset(curr,next,opts,false,false);opts.cssBefore.left=next.cycleW/2;opts.cssBefore.top=next.cycleH/2;$.extend(opts.animIn,{top:0,left:0,width:next.cycleW,height:next.cycleH});});opts.cssBefore.width=0;opts.cssBefore.height=0;opts.animOut.opacity=0;};$.fn.cycle.transitions.blindX=function($cont,$slides,opts){var w=$cont.css('overflow','hidden').width();opts.before.push(function(curr,next,opts){$.fn.cycle.commonReset(curr,next,opts);opts.animIn.width=next.cycleW;opts.animOut.left=curr.cycleW;});opts.cssBefore.left=w;opts.cssBefore.top=0;opts.animIn.left=0;opts.animOut.left=w;};$.fn.cycle.transitions.blindY=function($cont,$slides,opts){var h=$cont.css('overflow','hidden').height();opts.before.push(function(curr,next,opts){$.fn.cycle.commonReset(curr,next,opts);opts.animIn.height=next.cycleH;opts.animOut.top=curr.cycleH;});opts.cssBefore.top=h;opts.cssBefore.left=0;opts.animIn.top=0;opts.animOut.top=h;};$.fn.cycle.transitions.blindZ=function($cont,$slides,opts){var h=$cont.css('overflow','hidden').height();var w=$cont.width();opts.before.push(function(curr,next,opts){$.fn.cycle.commonReset(curr,next,opts);opts.animIn.height=next.cycleH;opts.animOut.top=curr.cycleH;});opts.cssBefore.top=h;opts.cssBefore.left=w;opts.animIn.top=0;opts.animIn.left=0;opts.animOut.top=h;opts.animOut.left=w;};$.fn.cycle.transitions.growX=function($cont,$slides,opts){opts.before.push(function(curr,next,opts){$.fn.cycle.commonReset(curr,next,opts,false,true);opts.cssBefore.left=this.cycleW/2;opts.animIn.left=0;opts.animIn.width=this.cycleW;opts.animOut.left=0;});opts.cssBefore.top=0;opts.cssBefore.width=0;};$.fn.cycle.transitions.growY=function($cont,$slides,opts){opts.before.push(function(curr,next,opts){$.fn.cycle.commonReset(curr,next,opts,true,false);opts.cssBefore.top=this.cycleH/2;opts.animIn.top=0;opts.animIn.height=this.cycleH;opts.animOut.top=0;});opts.cssBefore.height=0;opts.cssBefore.left=0;};$.fn.cycle.transitions.curtainX=function($cont,$slides,opts){opts.before.push(function(curr,next,opts){$.fn.cycle.commonReset(curr,next,opts,false,true,true);opts.cssBefore.left=next.cycleW/2;opts.animIn.left=0;opts.animIn.width=this.cycleW;opts.animOut.left=curr.cycleW/2;opts.animOut.width=0;});opts.cssBefore.top=0;opts.cssBefore.width=0;};$.fn.cycle.transitions.curtainY=function($cont,$slides,opts){opts.before.push(function(curr,next,opts){$.fn.cycle.commonReset(curr,next,opts,true,false,true);opts.cssBefore.top=next.cycleH/2;opts.animIn.top=0;opts.animIn.height=next.cycleH;opts.animOut.top=curr.cycleH/2;opts.animOut.height=0;});opts.cssBefore.height=0;opts.cssBefore.left=0;};$.fn.cycle.transitions.cover=function($cont,$slides,opts){var d=opts.direction||'left';var w=$cont.css('overflow','hidden').width();var h=$cont.height();opts.before.push(function(curr,next,opts){$.fn.cycle.commonReset(curr,next,opts);if(d=='right')
opts.cssBefore.left=-w;else if(d=='up')
opts.cssBefore.top=h;else if(d=='down')
opts.cssBefore.top=-h;else
opts.cssBefore.left=w;});opts.animIn.left=0;opts.animIn.top=0;opts.cssBefore.top=0;opts.cssBefore.left=0;};$.fn.cycle.transitions.uncover=function($cont,$slides,opts){var d=opts.direction||'left';var w=$cont.css('overflow','hidden').width();var h=$cont.height();opts.before.push(function(curr,next,opts){$.fn.cycle.commonReset(curr,next,opts,true,true,true);if(d=='right')
opts.animOut.left=w;else if(d=='up')
opts.animOut.top=-h;else if(d=='down')
opts.animOut.top=h;else
opts.animOut.left=-w;});opts.animIn.left=0;opts.animIn.top=0;opts.cssBefore.top=0;opts.cssBefore.left=0;};$.fn.cycle.transitions.toss=function($cont,$slides,opts){var w=$cont.css('overflow','visible').width();var h=$cont.height();opts.before.push(function(curr,next,opts){$.fn.cycle.commonReset(curr,next,opts,true,true,true);if(!opts.animOut.left&&!opts.animOut.top)
$.extend(opts.animOut,{left:w*2,top:-h/2,opacity:0});else
opts.animOut.opacity=0;});opts.cssBefore.left=0;opts.cssBefore.top=0;opts.animIn.left=0;};$.fn.cycle.transitions.wipe=function($cont,$slides,opts){var w=$cont.css('overflow','hidden').width();var h=$cont.height();opts.cssBefore=opts.cssBefore||{};var clip;if(opts.clip){if(/l2r/.test(opts.clip))
clip='rect(0px 0px '+h+'px 0px)';else if(/r2l/.test(opts.clip))
clip='rect(0px '+w+'px '+h+'px '+w+'px)';else if(/t2b/.test(opts.clip))
clip='rect(0px '+w+'px 0px 0px)';else if(/b2t/.test(opts.clip))
clip='rect('+h+'px '+w+'px '+h+'px 0px)';else if(/zoom/.test(opts.clip)){var top=parseInt(h/2,10);var left=parseInt(w/2,10);clip='rect('+top+'px '+left+'px '+top+'px '+left+'px)';}}
opts.cssBefore.clip=opts.cssBefore.clip||clip||'rect(0px 0px 0px 0px)';var d=opts.cssBefore.clip.match(/(\d+)/g);var t=parseInt(d[0],10),r=parseInt(d[1],10),b=parseInt(d[2],10),l=parseInt(d[3],10);opts.before.push(function(curr,next,opts){if(curr==next)return;var $curr=$(curr),$next=$(next);$.fn.cycle.commonReset(curr,next,opts,true,true,false);opts.cssAfter.display='block';var step=1,count=parseInt((opts.speedIn / 13),10)-1;(function f(){var tt=t?t-parseInt(step*(t/count),10):0;var ll=l?l-parseInt(step*(l/count),10):0;var bb=b<h?b+parseInt(step*((h-b)/count||1),10):h;var rr=r<w?r+parseInt(step*((w-r)/count||1),10):w;$next.css({clip:'rect('+tt+'px '+rr+'px '+bb+'px '+ll+'px)'});(step++<=count)?setTimeout(f,13):$curr.css('display','none');})();});$.extend(opts.cssBefore,{display:'block',opacity:1,top:0,left:0});opts.animIn={left:0};opts.animOut={left:0};};})(jQuery);;
/* Source and licensing information for the above line(s) can be found at https://palmettolasik.com/sites/all/libraries/jquery.cycle/jquery.cycle.all.js. */
/* Source and licensing information for the line(s) below can be found at https://palmettolasik.com/sites/all/modules/views_slideshow/contrib/views_slideshow_cycle/js/views_slideshow_cycle.js. */
(function($){Drupal.behaviors.viewsSlideshowCycle={attach:function(context){$('.views_slideshow_cycle_main:not(.viewsSlideshowCycle-processed)',context).addClass('viewsSlideshowCycle-processed').each(function(){var fullId='#'+$(this).attr('id');var settings=Drupal.settings.viewsSlideshowCycle[fullId];settings.targetId='#'+$(fullId+" :first").attr('id');settings.slideshowId=settings.targetId.replace('#views_slideshow_cycle_teaser_section_','');var pager_after_fn=function(curr,next,opts){var slideNum=opts.currSlide;if(typeof settings.processedAfter=='undefined'||!settings.processedAfter){settings.processedAfter=1;slideNum=(typeof settings.opts.startingSlide=='undefined')?0:settings.opts.startingSlide;}
if(settings.pause_after_slideshow){opts.counter+=1;if(opts.counter==settings.num_divs+1){opts.counter=1;Drupal.viewsSlideshow.action({"action":'pause',"slideshowID":settings.slideshowId,"force":true});}}
Drupal.viewsSlideshow.action({"action":'transitionEnd',"slideshowID":settings.slideshowId,"slideNum":slideNum});}
var pager_before_fn=function(curr,next,opts){$(document).trigger('drupal:views_slideshow_cycle:before',{curr:curr,next:next,opts:opts,settings:settings});var slideNum=opts.nextSlide;if(settings.remember_slide){createCookie(settings.vss_id,slideNum,settings.remember_slide_days);}
if(!settings.fixed_height){var $ht=$(next).height();$(next).parent().animate({height:$ht});}
if(typeof settings.processedBefore=='undefined'||!settings.processedBefore){settings.processedBefore=1;slideNum=(typeof opts.startingSlide=='undefined')?0:opts.startingSlide;}
Drupal.viewsSlideshow.action({"action":'transitionBegin',"slideshowID":settings.slideshowId,"slideNum":slideNum});}
settings.loaded=false;settings.opts={speed:settings.speed,timeout:settings.timeout,delay:settings.delay,sync:settings.sync,random:settings.random,nowrap:settings.nowrap,pause_after_slideshow:settings.pause_after_slideshow,counter:0,after:pager_after_fn,before:pager_before_fn,cleartype:(settings.cleartype)?true:false,cleartypeNoBg:(settings.cleartypenobg)?true:false}
if(settings.remember_slide){var startSlide=readCookie(settings.vss_id);if(startSlide==null){startSlide=0;}
settings.opts.startingSlide=parseInt(startSlide);}
if(settings.effect=='none'){settings.opts.speed=1;}
else{settings.opts.fx=settings.effect;}
var hash=location.hash;if(hash){var hash=hash.replace('#','');var aHash=hash.split(';');var aHashLen=aHash.length;for(var i=0;i<aHashLen;i++){var initialInfo=aHash[i].split(':');if(settings.slideshowId==initialInfo[0]&&settings.num_divs>initialInfo[1]){settings.opts.startingSlide=parseInt(initialInfo[1]);}}}
if(settings.pause){var mouseIn=function(){Drupal.viewsSlideshow.action({"action":'pause',"slideshowID":settings.slideshowId});}
var mouseOut=function(){Drupal.viewsSlideshow.action({"action":'play',"slideshowID":settings.slideshowId});}
if(jQuery.fn.hoverIntent){$('#views_slideshow_cycle_teaser_section_'+settings.vss_id).hoverIntent(mouseIn,mouseOut);}
else{$('#views_slideshow_cycle_teaser_section_'+settings.vss_id).hover(mouseIn,mouseOut);}}
if(settings.play_on_hover){var mouseIn=function(){Drupal.viewsSlideshow.action({"action":'play',"slideshowID":settings.slideshowId,"force":true});}
var mouseOut=function(){Drupal.viewsSlideshow.action({"action":'pause',"slideshowID":settings.slideshowId});}
if(jQuery.fn.hoverIntent){$('#views_slideshow_cycle_teaser_section_'+settings.vss_id).hoverIntent(mouseIn,mouseOut);}
else{$('#views_slideshow_cycle_teaser_section_'+settings.vss_id).hover(mouseIn,mouseOut);}}
if(settings.pause_on_click){$('#views_slideshow_cycle_teaser_section_'+settings.vss_id).click(function(){Drupal.viewsSlideshow.action({"action":'pause',"slideshowID":settings.slideshowId,"force":true});});}
if(typeof JSON!='undefined'){var advancedOptions=JSON.parse(settings.advanced_options);for(var option in advancedOptions){switch(option){case"activePagerClass":case"allowPagerClickBubble":case"autostop":case"autostopCount":case"backwards":case"bounce":case"cleartype":case"cleartypeNoBg":case"containerResize":case"continuous":case"delay":case"easeIn":case"easeOut":case"easing":case"fastOnEvent":case"fit":case"fx":case"manualTrump":case"metaAttr":case"next":case"nowrap":case"pager":case"pagerEvent":case"pause":case"pauseOnPagerHover":case"prev":case"prevNextEvent":case"random":case"randomizeEffects":case"requeueOnImageNotLoaded":case"requeueTimeout":case"rev":case"slideExpr":case"slideResize":case"speed":case"speedIn":case"speedOut":case"startingSlide":case"sync":case"timeout":var optionValue=advancedOptions[option];optionValue=Drupal.viewsSlideshowCycle.advancedOptionCleanup(optionValue);settings.opts[option]=optionValue;break;case"width":var optionValue=advancedOptions["width"];optionValue=Drupal.viewsSlideshowCycle.advancedOptionCleanup(optionValue);settings.opts["width"]=optionValue;settings.opts["containerResize"]=0;break;case"height":var optionValue=advancedOptions["height"];optionValue=Drupal.viewsSlideshowCycle.advancedOptionCleanup(optionValue);settings.opts["height"]=optionValue;settings.fixed_height=1;break;case"animIn":case"animInDelay":case"animOut":case"animOutDelay":case"cssBefore":case"cssAfter":case"shuffle":var cssValue=advancedOptions[option];cssValue=Drupal.viewsSlideshowCycle.advancedOptionCleanup(cssValue);settings.opts[option]=eval('('+cssValue+')');break;case"after":var afterValue=advancedOptions[option];afterValue=Drupal.viewsSlideshowCycle.advancedOptionCleanup(afterValue);settings.opts[option]=function(currSlideElement,nextSlideElement,options,forwardFlag){pager_after_fn(currSlideElement,nextSlideElement,options);eval(afterValue);}
break;case"before":var beforeValue=advancedOptions[option];beforeValue=Drupal.viewsSlideshowCycle.advancedOptionCleanup(beforeValue);settings.opts[option]=function(currSlideElement,nextSlideElement,options,forwardFlag){pager_before_fn(currSlideElement,nextSlideElement,options);eval(beforeValue);}
break;case"end":var endValue=advancedOptions[option];endValue=Drupal.viewsSlideshowCycle.advancedOptionCleanup(endValue);settings.opts[option]=function(options){eval(endValue);}
break;case"fxFn":var fxFnValue=advancedOptions[option];fxFnValue=Drupal.viewsSlideshowCycle.advancedOptionCleanup(fxFnValue);settings.opts[option]=function(currSlideElement,nextSlideElement,options,afterCalback,forwardFlag){eval(fxFnValue);}
break;case"onPagerEvent":var onPagerEventValue=advancedOptions[option];onPagerEventValue=Drupal.viewsSlideshowCycle.advancedOptionCleanup(onPagerEventValue);settings.opts[option]=function(zeroBasedSlideIndex,slideElement){eval(onPagerEventValue);}
break;case"onPrevNextEvent":var onPrevNextEventValue=advancedOptions[option];onPrevNextEventValue=Drupal.viewsSlideshowCycle.advancedOptionCleanup(onPrevNextEventValue);settings.opts[option]=function(isNext,zeroBasedSlideIndex,slideElement){eval(onPrevNextEventValue);}
break;case"pagerAnchorBuilder":var pagerAnchorBuilderValue=advancedOptions[option];pagerAnchorBuilderValue=Drupal.viewsSlideshowCycle.advancedOptionCleanup(pagerAnchorBuilderValue);settings.opts[option]=function(index,DOMelement){var returnVal='';eval(pagerAnchorBuilderValue);return returnVal;}
break;case"pagerClick":var pagerClickValue=advancedOptions[option];pagerClickValue=Drupal.viewsSlideshowCycle.advancedOptionCleanup(pagerClickValue);settings.opts[option]=function(zeroBasedSlideIndex,slideElement){eval(pagerClickValue);}
break;case"paused":var pausedValue=advancedOptions[option];pausedValue=Drupal.viewsSlideshowCycle.advancedOptionCleanup(pausedValue);settings.opts[option]=function(cont,opts,byHover){eval(pausedValue);}
break;case"resumed":var resumedValue=advancedOptions[option];resumedValue=Drupal.viewsSlideshowCycle.advancedOptionCleanup(resumedValue);settings.opts[option]=function(cont,opts,byHover){eval(resumedValue);}
break;case"timeoutFn":var timeoutFnValue=advancedOptions[option];timeoutFnValue=Drupal.viewsSlideshowCycle.advancedOptionCleanup(timeoutFnValue);settings.opts[option]=function(currSlideElement,nextSlideElement,options,forwardFlag){var returnVal=settings.timeout;eval(timeoutFnValue);return returnVal;}
break;case"updateActivePagerLink":var updateActivePagerLinkValue=advancedOptions[option];updateActivePagerLinkValue=Drupal.viewsSlideshowCycle.advancedOptionCleanup(updateActivePagerLinkValue);settings.opts[option]=function(pager,currSlideIndex){eval(updateActivePagerLinkValue);}
break;}}}
if(settings.wait_for_image_load){settings.totalImages=$(settings.targetId+' img').length;if(settings.totalImages){settings.loadedImages=0;$(settings.targetId+' img').each(function(){var $imageElement=$(this);$imageElement.bind('load',function(){Drupal.viewsSlideshowCycle.imageWait(fullId);});var imgSrc=$imageElement.attr('src');$imageElement.attr('src','');$imageElement.attr('src',imgSrc);});setTimeout("Drupal.viewsSlideshowCycle.load('"+fullId+"')",settings.wait_for_image_load_timeout);}
else{Drupal.viewsSlideshowCycle.load(fullId);}}
else{Drupal.viewsSlideshowCycle.load(fullId);}});}};Drupal.behaviors.viewsSlideshowSwipe={attach:function(context){var isTouch=(('ontouchstart'in window)||(navigator.msMaxTouchPoints>0));if(isTouch===true&&$('.views-slideshow-cycle-main-frame').length){var $slider=$('.views-slideshow-cycle-main-frame'),opts={start:{x:0,y:0},end:{x:0,y:0},hdiff:0,vdiff:0,length:0,angle:null,direction:null,},optsReset=$.extend(true,{},opts),H_THRESHOLD=110,V_THRESHOLD=50;$slider.data('bw',opts).bind('touchstart.cycle',function(e){var touch=e.originalEvent.touches[0]||e.originalEvent.changedTouches[0];if(e.originalEvent.touches.length==1){var data=$(this).data('bw');data.start.x=touch.pageX;data.start.y=touch.pageY;$(this).data('bw',data);}}).bind('touchend.cycle',function(e){var touch=e.originalEvent.touches[0]||e.originalEvent.changedTouches[0];var data=$(this).data('bw');data.end.x=touch.pageX;data.end.y=touch.pageY;$(this).data('bw',data);if(data.start.x!=0&&data.start.y!=0){data.vdiff=data.start.x-data.end.x;data.hdiff=data.end.y-data.start.y;if(Math.abs(data.vdiff)==data.start.x&&Math.abs(data.hdiff)==data.start.y){data.vdiff=0;data.hdiff=0;}
var length=Math.round(Math.sqrt(Math.pow(data.vdiff,2)+Math.pow(data.hdiff,2)));var rads=Math.atan2(data.hdiff,data.vdiff);var angle=Math.round(rads*180/Math.PI);if(angle<0){angle=360-Math.abs(angle);}
if(length>H_THRESHOLD&&V_THRESHOLD>data.hdiff){e.preventDefault();if(angle>135&&angle<225){var cyopt=$slider.data('cycle.opts');if(cyopt.currSlide>0){$slider.cycle((cyopt.currSlide-1),'scrollRight');}
else{$slider.cycle((cyopt.slideCount-1),'scrollRight');}}
else if(angle>315||angle<45){$slider.cycle('next');}}}
data=$.extend(true,{},optsReset);});}}};Drupal.viewsSlideshowCycle=Drupal.viewsSlideshowCycle||{};Drupal.viewsSlideshowCycle.advancedOptionCleanup=function(value){value=$.trim(value);value=value.replace(/\n/g,'');if(value.match(/^[\d.]+%$/)){}
else if(!isNaN(parseInt(value))){value=parseInt(value);}
else if(value.toLowerCase()=='true'){value=true;}
else if(value.toLowerCase()=='false'){value=false;}
return value;}
Drupal.viewsSlideshowCycle.imageWait=function(fullId){if(++Drupal.settings.viewsSlideshowCycle[fullId].loadedImages==Drupal.settings.viewsSlideshowCycle[fullId].totalImages){Drupal.viewsSlideshowCycle.load(fullId);}};Drupal.viewsSlideshowCycle.load=function(fullId){var settings=Drupal.settings.viewsSlideshowCycle[fullId];if(!settings.loaded){$(settings.targetId).cycle(settings.opts);$(settings.targetId).parent().parent().addClass('views-slideshow-cycle-processed');settings.loaded=true;if(settings.start_paused){Drupal.viewsSlideshow.action({"action":'pause',"slideshowID":settings.slideshowId,"force":true});}
if(settings.pause_when_hidden){var checkPause=function(settings){var visible=viewsSlideshowCycleIsVisible(settings.targetId,settings.pause_when_hidden_type,settings.amount_allowed_visible);if(visible){Drupal.viewsSlideshow.action({"action":'play',"slideshowID":settings.slideshowId});}
else{Drupal.viewsSlideshow.action({"action":'pause',"slideshowID":settings.slideshowId});}}
$(window).scroll(function(){checkPause(settings);});$(window).resize(function(){checkPause(settings);});}}};Drupal.viewsSlideshowCycle.pause=function(options){try{if(options.pause_in_middle&&$.fn.pause){$('#views_slideshow_cycle_teaser_section_'+options.slideshowID).pause();}
else{$('#views_slideshow_cycle_teaser_section_'+options.slideshowID).cycle('pause');}}
catch(e){if(!e instanceof TypeError){throw e;}}};Drupal.viewsSlideshowCycle.play=function(options){Drupal.settings.viewsSlideshowCycle['#views_slideshow_cycle_main_'+options.slideshowID].paused=false;if(options.pause_in_middle&&$.fn.resume){$('#views_slideshow_cycle_teaser_section_'+options.slideshowID).resume();}
else{$('#views_slideshow_cycle_teaser_section_'+options.slideshowID).cycle('resume');}};Drupal.viewsSlideshowCycle.previousSlide=function(options){$('#views_slideshow_cycle_teaser_section_'+options.slideshowID).cycle('prev');};Drupal.viewsSlideshowCycle.nextSlide=function(options){$('#views_slideshow_cycle_teaser_section_'+options.slideshowID).cycle('next');};Drupal.viewsSlideshowCycle.goToSlide=function(options){$('#views_slideshow_cycle_teaser_section_'+options.slideshowID).cycle(options.slideNum);};function IsNumeric(sText){var ValidChars="0123456789";var IsNumber=true;var Char;for(var i=0;i<sText.length&&IsNumber==true;i++){Char=sText.charAt(i);if(ValidChars.indexOf(Char)==-1){IsNumber=false;}}
return IsNumber;}
function createCookie(name,value,days){if(days){var date=new Date();date.setTime(date.getTime()+(days*24*60*60*1000));var expires="; expires="+date.toGMTString();}
else{var expires="";}
document.cookie=name+"="+value+expires+"; path=/";}
function readCookie(name){var nameEQ=name+"=";var ca=document.cookie.split(';');for(var i=0;i<ca.length;i++){var c=ca[i];while(c.charAt(0)==' ')c=c.substring(1,c.length);if(c.indexOf(nameEQ)==0){return c.substring(nameEQ.length,c.length);}}
return null;}
function eraseCookie(name){createCookie(name,"",-1);}
function viewsSlideshowCycleIsVisible(elem,type,amountVisible){var docViewTop=$(window).scrollTop();var docViewBottom=docViewTop+$(window).height();var docViewLeft=$(window).scrollLeft();var docViewRight=docViewLeft+$(window).width();var elemTop=$(elem).offset().top;var elemHeight=$(elem).height();var elemBottom=elemTop+elemHeight;var elemLeft=$(elem).offset().left;var elemWidth=$(elem).width();var elemRight=elemLeft+elemWidth;var elemArea=elemHeight*elemWidth;var missingLeft=0;var missingRight=0;var missingTop=0;var missingBottom=0;if(elemLeft<docViewLeft){missingLeft=docViewLeft-elemLeft;}
if(elemRight>docViewRight){missingRight=elemRight-docViewRight;}
if(elemTop<docViewTop){missingTop=docViewTop-elemTop;}
if(elemBottom>docViewBottom){missingBottom=elemBottom-docViewBottom;}
if(type=='full'){return((elemBottom>=docViewTop)&&(elemTop<=docViewBottom)&&(elemBottom<=docViewBottom)&&(elemTop>=docViewTop)&&(elemLeft>=docViewLeft)&&(elemRight<=docViewRight)&&(elemLeft<=docViewRight)&&(elemRight>=docViewLeft));}
else if(type=='vertical'){var verticalShowing=elemHeight-missingTop-missingBottom;if(typeof amountVisible==='string'&&amountVisible.indexOf('%')){return(((verticalShowing/elemHeight)*100)>=parseInt(amountVisible));}
else{return(verticalShowing>=parseInt(amountVisible));}}
else if(type=='horizontal'){var horizontalShowing=elemWidth-missingLeft-missingRight;if(typeof amountVisible==='string'&&amountVisible.indexOf('%')){return(((horizontalShowing/elemWidth)*100)>=parseInt(amountVisible));}
else{return(horizontalShowing>=parseInt(amountVisible));}}
else if(type=='area'){var areaShowing=(elemWidth-missingLeft-missingRight)*(elemHeight-missingTop-missingBottom);if(typeof amountVisible==='string'&&amountVisible.indexOf('%')){return(((areaShowing/elemArea)*100)>=parseInt(amountVisible));}
else{return(areaShowing>=parseInt(amountVisible));}}}})(jQuery);;
/* Source and licensing information for the above line(s) can be found at https://palmettolasik.com/sites/all/modules/views_slideshow/contrib/views_slideshow_cycle/js/views_slideshow_cycle.js. */
